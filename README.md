# Personal Site

The site is in the form of a server-side rendered app built with [Leptos](https://leptos.dev), served by [Nginx](https://nginx.org), and deployed via [Ansible](https://www.ansible.com/). Previous versions can be accessed by tag: [Zola](https://codeberg.org/kdwarn/kdwarn.net/src/tag/zola-0.2), [Django](https://codeberg.org/kdwarn/kdwarn.net/src/tag/django), and [Dioxus](https://codeberg.org/kdwarn/kdwarn.net/src/tag/dioxus).

For local development, run `cargo leptos watch`. The `--release` flag can be used not just for using release version but also final size of wasm file. The README on the Ansible project contains production deployment steps.

I'll need to script this in some way, but since I don't want to waste Codeberg's storage on my pictures for the outdoors section of the site, I'm manually transferring them to the server right now. They should go into static/images/outdoors, and that should be done before the build, because the build will copy them from there to where they will actually be served from, target/site/images/outdoors.

Also need to script resizing/compressing images. Using `convert <image-file> -quality 90 -resize <size> <new-file>`. 600 or 800 for [size](https://legacy.imagemagick.org/script/command-line-processing.php#geometry) (depending on orientation).
