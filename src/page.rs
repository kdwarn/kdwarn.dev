use std::fmt::{self, Display, Formatter};
use std::fs::read_to_string;

use comrak::{markdown_to_html, Options};
use leptos::{component, prelude::*, view, IntoView};
use leptos_meta::{Meta, Title};
use serde::{Deserialize, Serialize};

use crate::app::NotFound;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub enum Page {
    Home,
    Projects,
    Timestudy,
    Taskfinder,
    Links,
    Feeds,
}

impl Display for Page {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let path = match &self {
            Page::Home => "content/home.md",
            Page::Projects => "content/projects/_index.md",
            Page::Timestudy => "content/projects/timestudy.md",
            Page::Taskfinder => "content/projects/taskfinder.md",
            Page::Links => "content/links.md",
            Page::Feeds => "content/feeds.md",
        };
        write!(f, "{:#}", path)
    }
}

impl Page {
    fn title(&self) -> String {
        match self {
            Page::Home => "kdwarn".to_string(),
            _ => format!("{:?} | kdwarn", self),
        }
    }
    fn description(&self) -> &'static str {
        match self {
            Page::Home => "Free and open source software, gardening, and a little poetry.",
            Page::Projects => "My software projects.",
            Page::Timestudy => "Timestudy CLI is a tool to track your activities on the command line, heavily inspired by Timewarrior.",
            Page::Taskfinder => "Keep your project-related tasks where they belong - with your notes on the project! This program, a terminal user interface (TUI), will extract and display them.",
            Page::Links => "Links I've collected.",
            Page::Feeds => "Feeds for various kinds of my posts.",
        }
    }
}

#[component]
pub fn ViewPage(page: Page) -> impl IntoView {
    let md_content = match read_to_string(format!("{page}")) {
        Ok(v) => {
            let mut md_options = Options::default();
            md_options.extension.header_ids = Some("".to_string());
            md_options.extension.autolink = true;
            md_options.render.unsafe_ = true;
            markdown_to_html(&v, &md_options)
        }
        Err(_) => return NotFound().into_any(),
    };

    view! {
        <Title text={page.title()}/>
        <Meta
            name="description"
            content=page.description()
        />
        <div class="content-body" inner_html=md_content/>
    }
    .into_any()
}
