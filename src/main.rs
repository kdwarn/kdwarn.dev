#[cfg(feature = "ssr")]
use kdwarn::posts::{feed, MarkdownPost, POSTS};

#[cfg(feature = "ssr")]
use axum::response::IntoResponse;

#[cfg(feature = "ssr")]
#[tokio::main]
async fn main() {
    use axum::{routing::get, Router};
    use kdwarn::app::{shell, App};
    use leptos::{logging, prelude::*};
    use leptos_axum::{generate_route_list, LeptosRoutes};
    // Setting get_configuration(None) means we'll be using cargo-leptos's env values
    // For deployment these variables are:
    // <https://github.com/leptos-rs/start-axum#executing-a-server-on-a-remote-machine-without-the-toolchain>
    // Alternately a file can be specified such as Some("Cargo.toml")
    // The file would need to be included with the executable when moved to deployment
    let conf = get_configuration(None).unwrap();
    let leptos_options = conf.leptos_options;
    let addr = leptos_options.site_addr;
    let routes = generate_route_list(App);

    // build our application with a route
    let app = Router::new()
        .route("/blog/feed.xml", get(blog_feed))
        .route("/blog/tags/coding/feed.xml", get(coding_feed))
        .route("/blog/tags/daybook/feed.xml", get(daybook_feed))
        .route("/blog/tags/outdoors/feed.xml", get(outdoors_feed))
        .route("/poetry/feed.xml", get(poetry_feed))
        .leptos_routes(&leptos_options, routes, {
            let leptos_options = leptos_options.clone();
            move || shell(leptos_options.clone())
        })
        .fallback(leptos_axum::file_and_error_handler(shell))
        .with_state(leptos_options);

    let listener = tokio::net::TcpListener::bind(&addr).await.unwrap();
    logging::log!("listening on http://{}", &addr);
    axum::serve(listener, app.into_make_service())
        .await
        .unwrap();
}

#[cfg(feature = "ssr")]
async fn blog_feed() -> impl axum::response::IntoResponse {
    feed::generate_feed(
        "kdwarn: blog",
        "Blog about software and gardening, mostly.",
        "https://kdwarn.net/blog/feed.xml",
        POSTS.blog_posts.clone(),
    )
}

#[cfg(feature = "ssr")]
async fn coding_feed() -> impl axum::response::IntoResponse {
    let mut posts = POSTS.blog_posts.clone();
    posts.retain(|p| match p {
        MarkdownPost::BlogPost(blog) => blog.front_matter.tags.contains(&"coding".to_string()),
        MarkdownPost::Poem(_) => false,
    });

    feed::generate_feed(
        "kdwarn: coding",
        "Coding blog posts",
        "https://kdwarn.net/blog/tags/coding/feed.xml",
        posts,
    )
}

#[cfg(feature = "ssr")]
async fn daybook_feed() -> impl IntoResponse {
    let mut posts = POSTS.blog_posts.clone();
    posts.retain(|p| match p {
        MarkdownPost::BlogPost(blog) => blog.front_matter.tags.contains(&"daybook".to_string()),
        MarkdownPost::Poem(_) => false,
    });

    feed::generate_feed(
        "kdwarn: daybook",
        "Daybook",
        "https://kdwarn.net/blog/tags/daybook/feed.xml",
        posts,
    )
}

#[cfg(feature = "ssr")]
async fn outdoors_feed() -> impl IntoResponse {
    let mut posts = POSTS.blog_posts.clone();
    posts.retain(|p| match p {
        MarkdownPost::BlogPost(blog) => blog.front_matter.tags.contains(&"outdoors".to_string()),
        MarkdownPost::Poem(_) => false,
    });

    feed::generate_feed(
        "kdwarn: outdoors",
        "Outdoors blog posts",
        "https://kdwarn.net/blog/tags/outdoors/feed.xml",
        posts,
    )
}

#[cfg(feature = "ssr")]
async fn poetry_feed() -> impl IntoResponse {
    feed::generate_feed(
        "kdwarn: poetry",
        "Poems",
        "https://kdwarn.net/poetry/feed.xml",
        POSTS.poems.clone(),
    )
}

#[cfg(not(feature = "ssr"))]
pub fn main() {
    // no client-side main function
    // unless we want this to work with e.g., Trunk for a purely client-side app
    // see lib.rs for hydration function instead
}
