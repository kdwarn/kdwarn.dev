use chrono::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Poem {
    pub front_matter: PoemFrontMatter,
    pub filestem: String,
    pub body: String,
}

impl Poem {
    pub fn new(front_matter: PoemFrontMatter, filestem: String, body: String) -> Self {
        Self {
            front_matter,
            filestem,
            body,
        }
    }
}

#[derive(Deserialize, Debug, Serialize, Clone)]
pub struct PoemFrontMatter {
    pub title: String,
    pub date: NaiveDate,
}
