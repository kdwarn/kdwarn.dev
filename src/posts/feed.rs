//! Create RSS/Atom feeds.
//! This is (slightly) adapted from <https://benw.is/posts/rss-in-axum>.
use crate::posts::{MarkdownPost, PostKind};
use chrono::{FixedOffset, NaiveTime};

pub struct FeedEntry {
    pub title: String,
    pub link: String,
    pub description: Option<String>,
    pub pub_date: String,
    pub author: String,
    pub guid: String,
}

impl From<MarkdownPost> for FeedEntry {
    fn from(post: MarkdownPost) -> Self {
        let mut url = "https://kdwarn.net".to_string();
        let title;
        let description;
        let pub_date;
        let author = "Kris Warner".to_string();
        let guid;

        // MarkdownPosts just use NaiveDate, but we need DateTime
        // (and ultimately in RFC2822 format).
        let tz = FixedOffset::east_opt(5 * 3600).unwrap();
        let hour = 9; // 9am seems fine to use for a time.
        match post {
            MarkdownPost::BlogPost(p) => {
                url = format!("{url}{}{}", PostKind::BlogPost.posts_path(), p.filestem);
                title = p.front_matter.title;
                description = Some(p.body);
                pub_date = p
                    .front_matter
                    .date
                    .and_time(NaiveTime::from_hms_opt(hour, 0, 0).unwrap())
                    .and_local_timezone(tz)
                    .unwrap()
                    .to_rfc2822();
                guid = url.clone();
            }
            MarkdownPost::Poem(p) => {
                url = format!("{url}{}{}", PostKind::Poem.posts_path(), p.filestem);
                title = p.front_matter.title;
                description = Some(p.body);
                pub_date = p
                    .front_matter
                    .date
                    .and_time(NaiveTime::from_hms_opt(hour, 0, 0).unwrap())
                    .and_local_timezone(tz)
                    .unwrap()
                    .to_rfc2822();
                guid = url.clone();
            }
        };

        Self {
            title,
            link: url,
            description,
            pub_date,
            author,
            guid,
        }
    }
}

impl FeedEntry {
    // Converts an FeedEntry to a String containing the entry item tags
    pub fn to_item(&self) -> String {
        format!(
            r#"
        <item>
            <title><![CDATA[{}]]></title>
            <description><![CDATA[{}]]></description>
            <pubDate>{}</pubDate>
            <link>{}</link>
            <guid isPermaLink="true">{}</guid>
        </item>
      "#,
            self.title,
            self.description.clone().unwrap_or_default(),
            self.pub_date,
            self.link,
            self.guid
        )
    }
}

pub fn generate_feed(
    title: &str,
    description: &str,
    link: &str,
    posts: Vec<MarkdownPost>,
) -> String {
    // Generate XML tags for Posts and collect them into a single string.
    let feed_entries = posts
        .iter()
        .map(|p| FeedEntry::from(p.clone()))
        .map(|r: FeedEntry| r.to_item())
        .collect::<String>();

    format!(
        r#"<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title>{title}</title>
        <description>{description}</description>
        <link>{link}</link>
        <language>en-us</language>
        <ttl>60</ttl>
        <atom:link href="{link}" rel="self" type="application/rss+xml" />
        {}
    </channel>
</rss>   
     "#,
        feed_entries
    )
}
