use chrono::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct BlogPost {
    pub front_matter: BlogFrontMatter,
    pub filestem: String,
    pub body: String,
}

impl BlogPost {
    pub fn new(front_matter: BlogFrontMatter, filestem: String, body: String) -> Self {
        Self {
            front_matter,
            filestem,
            body,
        }
    }
}

#[derive(Deserialize, Debug, Serialize, Clone)]
pub struct BlogFrontMatter {
    pub title: String,
    pub date: NaiveDate,
    pub tags: Vec<String>,
    pub image: Option<String>,
}
