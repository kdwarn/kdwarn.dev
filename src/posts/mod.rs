use std::fs::{self, read_to_string};
use std::sync::LazyLock;

use comrak::{
    markdown_to_html_with_plugins, nodes::NodeValue, parse_document,
    plugins::syntect::SyntectAdapter, Arena, ComrakPlugins, Options,
};
use leptos::{prelude::*, either::Either};
use leptos_meta::{Meta, Title};
use leptos_router::{components::*, hooks::use_params, params::Params};
use serde::{Deserialize, Serialize};

use crate::app::{FeedIcon, NotFound};

pub mod blog;
pub mod feed;
pub mod poetry;

use blog::{BlogFrontMatter, BlogPost};
use poetry::{Poem, PoemFrontMatter};

pub const DT_FORMAT: &str = "%B %e, %Y";

static SYNTECT_ADAPTER: LazyLock<SyntectAdapter> = LazyLock::new(|| SyntectAdapter::new(None));

pub static POSTS: LazyLock<Content> = LazyLock::new(|| match Content::new() {
    Ok(v) => v,
    Err(e) => {
        panic!("Error getting posts: {}", e)
    }
});

/// All posts and their content, held in `POSTS` above.
pub struct Content {
    pub blog_posts: Vec<MarkdownPost>,
    pub blog_tags: Vec<String>,
    pub poems: Vec<MarkdownPost>,
}

impl Content {
    /// Extract all types of post content.
    ///
    /// Called with LazyLock, it only happens once throughout life of app.
    fn new() -> Result<Self, std::io::Error> {
        fn extract_content(
            kind: PostKind,
        ) -> Result<(Vec<MarkdownPost>, Vec<String>), std::io::Error> {
            let mut posts = vec![];
            for entry in fs::read_dir(kind.content_dir())? {
                let entry = entry?;
                let path = entry.path();
                if path.is_file() && path.extension().is_some_and(|v| v == "md") {
                    let post = parse_content(
                        path.file_stem().unwrap().to_str().unwrap().to_string(),
                        &read_to_string(&path)?,
                        kind,
                    );
                    posts.push(post)
                }
            }

            posts.sort_unstable_by_key(|p| match p {
                MarkdownPost::BlogPost(p) => p.front_matter.date,
                MarkdownPost::Poem(p) => p.front_matter.date,
            });
            posts.reverse();

            let mut tags = posts
                .clone()
                .into_iter()
                .flat_map(|p| match p {
                    MarkdownPost::BlogPost(p) => p.front_matter.tags,
                    MarkdownPost::Poem(_) => vec![],
                })
                .collect::<Vec<_>>();
            tags.sort();
            tags.dedup();

            Ok((posts, tags))
        }

        let (blog_posts, blog_tags) = extract_content(PostKind::BlogPost)?;
        let (poems, _) = extract_content(PostKind::Poem)?;

        Ok(Content {
            blog_posts,
            blog_tags,
            poems,
        })
    }
}

#[derive(Deserialize, Debug, Serialize, Clone, Copy, PartialEq)]
pub enum PostKind {
    BlogPost,
    Poem,
}

impl<'a> PostKind {
    pub fn content_dir(&self) -> &'a str {
        match self {
            PostKind::BlogPost => "content/blog/",
            PostKind::Poem => "content/poetry/",
        }
    }
    pub fn posts_path(self) -> &'a str {
        match self {
            PostKind::BlogPost => "/blog/",
            PostKind::Poem => "/poetry/",
        }
    }
    pub fn tags_path(&self) -> &'a str {
        match self {
            PostKind::BlogPost => "/blog/tags/",
            PostKind::Poem => "",
        }
    }
    pub fn heading(&self) -> &'a str {
        match self {
            PostKind::BlogPost => "Blog",
            PostKind::Poem => "Poetry",
        }
    }
    pub fn intro(&self) -> AnyView {
        match self {
            PostKind::BlogPost => view! {
                <Meta name="description" content="Blogging about coding, things I learn, gardening, and wildlife."/>
                <p>"There are three broad categories of posts here, which can be found via the"
                " following tags: "<A href="/blog/tags/coding">"coding"</A>" for blog posts about"
                " coding, "<A href="/blog/tags/daybook">"daybook"</A>" for my \"daybook\""
                " (brief posts about something I learned related to coding or Linux or using"
                " various programs), and "
                <A href="/blog/tags/outdoors">"outdoors"</A>" for all posts about gardening"
                " and wildlife."</p>
                <p><FeedIcon/>" Feeds available "<A href="/feeds">"here"</A>"."</p>
            }
            .into_any(),
            PostKind::Poem => view! {
                <Meta name="description" content="Selected poems I've written."/>
            }.into_any(),                
        }
    }
}

#[derive(Deserialize, Debug, Serialize, Clone)]
pub enum MarkdownPost {
    BlogPost(BlogPost),
    Poem(Poem),
}

impl MarkdownPost {
    fn list_view(self) -> impl IntoView {
        match self {
            Self::BlogPost(p) => view! {
                <Title text="Blog | kdwarn"/>
                <h2>
                    <a href=format!("{}{}", PostKind::BlogPost.posts_path(), p.filestem)>{ p.front_matter.title }</a>
                </h2>
                <p>{ format!("{}", p.front_matter.date.format(DT_FORMAT)) } <br/>
                    <span class="small">
                        { display_tags(p.front_matter.tags, PostKind::BlogPost).into_any() }
                    </span>
                </p>
            }.into_any(),
            Self::Poem(p) => view! {
                <Title text="Poetry | kdwarn"/>
                <h2>
                    <a href=format!("{}{}", PostKind::Poem.posts_path(), p.filestem)>{ p.front_matter.title }</a>
                </h2>
                <p>{ format!("{}", p.front_matter.date.format(DT_FORMAT)) }</p>
            }.into_any(),
        }
    }
}

impl IntoRender for MarkdownPost {
    type Output = AnyView;
    fn into_render(self) -> Self::Output {
        match self {
            Self::BlogPost(p) => view! {
                {if let Some(v) = p.front_matter.image {
                    Either::Left(view! {<Meta name="og:image" content={v} />})
                } else {
                    Either::Right(view! {})
                }}
                <Meta name="fediverse:creator" content="@kdwarn@fosstodon.org" />
                <Title text={ format!("{} | kdwarn", p.front_matter.title.clone()) }/>
                <article>
                    <h1> { p.front_matter.title }</h1>
                    <div class="content-body">
                        <p> { format!("{}", p.front_matter.date.format(DT_FORMAT)) } <br/>
                            <span class="small">
                                { display_tags(p.front_matter.tags, PostKind::BlogPost) }
                            </span>
                        </p>
                        <div inner_html=p.body />
                    </div>
                </article>
            }
            .into_any(),
            Self::Poem(p) => view! {
                <Title text={ format!("{} | kdwarn", p.front_matter.title.clone()) }/>
                <article>
                    <h1> { p.front_matter.title }</h1>
                    <div class="content-body">
                        <p> { format!("{}", p.front_matter.date.format(DT_FORMAT)) } </p>
                        <div inner_html=p.body />
                    </div>
                </article>
            }
            .into_any(),
        }
    }
}

#[derive(Deserialize, Debug, Serialize, Clone)]
pub enum FrontMatter {
    BlogPost(BlogFrontMatter),
    Poem(PoemFrontMatter),
}

#[derive(Params, PartialEq)]
struct PostParams {
    slug: String,
}

#[component]
pub fn Post(kind: PostKind) -> impl IntoView {
    let params = use_params::<PostParams>();
    let slug = move || {
        params.with(|params| {
            params
                .as_ref()
                .map(|params| params.slug.clone())
                .unwrap_or_default()
        })
    };

    match get_post(kind, slug()) {
        Some(v) => v.clone().into_render(),
        None => NotFound().into_any(),
    }
}

#[derive(Params, PartialEq, Debug)]
struct PostsParams {
    tag: Option<String>,
}

#[component]
pub fn Posts(kind: PostKind) -> impl IntoView {
    let params = use_params::<PostsParams>();
    let tag = move || {
        params.with(|params| {
            params
                .as_ref()
                .map(|params| params.tag.clone())
                .unwrap_or_default()
        })
    };

    let intro_or_tagged = move || match tag() {
        Some(v) => {
            let mut tagged = vec![view! { <p>"tagged: "{v.clone()}</p> }.into_any()];
            if &v == "coding" {
                tagged.push(
                    view! {
                        <p><FeedIcon/>
                        " A feed for these posts is available "
                        <a href="/blog/tags/coding/feed.xml" rel="external">"here"</a>"."</p>
                    }
                    .into_any(),
                );
            } else if &v == "daybook" {
                tagged.push(
                    view! {
                        <p>"In "<a href="https://en.wikipedia.org/wiki/The_Pragmatic_Programmer">The Pragmatic Programmer</a>", Andy Hunt and Dave Thomas recommend keeping a \"daybook\", which they had picked up from electronic and mechanical engineers, who used it as \"a kind of journal in which they recorded what they did, things they'd learned, sketches of ideas, readings from meters: basically anything to do with their work.\" They note three benefits, which I'll shorten a bit:"</p>
                        <ol>
                            <li>"\"It is more reliable than memory.\""</li>
                            <li>"\"It gives you a place to store ideas that aren't immediately relevant to the task at hand.\""</li>
                            <li>"\"It acts as a kind of rubber duck [...].\""</li>
                        </ol>
                        <p>"They explicitly recommend \"paper, not a file or a wiki\", but here's my digital version anyway."</p>

                        <p><FeedIcon/>
                        " A feed for these posts is available "
                        <a href="/blog/tags/daybook/feed.xml" rel="external">"here"</a>"."</p>
                    }
                    .into_any(),
                );
            } else if &v == "outdoors" {
                tagged.push(
                    view! {
                        <p>"Gardening, tree planting, yardwork, wildlife, nature, etc. USDA hardiness zone 7b, as of the remapping in 2023. Dates are usually when I did something, not necessarily when I wrote about it."</p>
                        <p><FeedIcon/>
                        " A feed for these posts is available "
                        <a href="/blog/tags/outdoors/feed.xml" rel="external">"here"</a>"."</p>
                        <p>"I've collected related links "<A href="/links#outdoors">"here"</A>"."</p>
                    }
                    .into_any(),
                );
            }
            tagged.into_any()
        }
        None => kind.intro(),
    };

    let (posts, tags) = get_posts(kind, tag());

    view! {
        <section>
            <div class="content-body">
                <h1>{ kind.heading() }</h1>
                <p>{ intro_or_tagged }</p>
                {posts.into_iter().map(|post| view! { {post.list_view()}}).collect::<Vec<_>>()}
            </div>
            <div class="extras">
                { if kind != PostKind::Poem {
                    view! {
                        <h3>All Tags</h3>
                        { display_tags(tags.clone(), kind) }
                    }.into_any()
                } else {
                    "".into_any()
                }}
            </div>
        </section>
    }
    .into_any()
}

/// Get one post.
fn get_post(kind: PostKind, slug: String) -> Option<MarkdownPost> {
    let posts = match kind {
        PostKind::BlogPost => POSTS.blog_posts.clone(),
        PostKind::Poem => POSTS.poems.clone(),
    };

    let mut md_post: Option<MarkdownPost> = None;
    for post in posts {
        match post {
            MarkdownPost::BlogPost(ref p) => {
                if p.filestem == slug.clone() {
                    md_post = Some(post.clone());
                    break;
                }
            }
            MarkdownPost::Poem(ref p) => {
                if p.filestem == slug.clone() {
                    md_post = Some(post);
                    break;
                }
            }
        }
    }
    md_post
}

/// Get all posts.
fn get_posts(kind: PostKind, tag: Option<String>) -> (Vec<MarkdownPost>, Vec<String>) {
    let (mut posts, tags) = match kind {
        PostKind::BlogPost => (POSTS.blog_posts.clone(), POSTS.blog_tags.clone()),
        PostKind::Poem => (POSTS.poems.clone(), vec![]),
    };

    if let Some(v) = tag {
        posts = posts
            .into_iter()
            .filter(|post| match post {
                MarkdownPost::BlogPost(p) => p.front_matter.tags.contains(&v),
                MarkdownPost::Poem(_) => false,
            })
            .collect::<Vec<_>>();
    }
    (posts, tags)
}

pub fn parse_content(filestem: String, content: &str, kind: PostKind) -> MarkdownPost {
    // Set comrak options to use frontmatter (it will be ignored by the converter).
    let mut md_options = Options::default();
    md_options.extension.autolink = true;
    md_options.render.unsafe_ = true;
    md_options.extension.front_matter_delimiter = Some("+++".to_owned());

    // In poems, soft line breaks should be converted to hard line breaks.
    if kind == PostKind::Poem {
        md_options.render.hardbreaks = true;
    }
    // Use syntect plugin for code syntax highlighting.
    let mut plugins = ComrakPlugins::default();
    plugins.render.codefence_syntax_highlighter = Some(&*SYNTECT_ADAPTER);

    // Generate html from body of file.
    let html = markdown_to_html_with_plugins(content, &md_options, &plugins);

    // Get the frontmatter.
    let front_matter = parse_front_matter(content, kind);

    match front_matter {
        FrontMatter::BlogPost(fm) => MarkdownPost::BlogPost(BlogPost::new(fm, filestem, html)),
        FrontMatter::Poem(fm) => MarkdownPost::Poem(Poem::new(fm, filestem, html)),
    }
}

/// Strip whitespace and frontmatter delimiters from string and parse resulting toml.
pub fn parse_front_matter(text: &str, kind: PostKind) -> FrontMatter {
    // Set comrak options to use frontmatter (it will be ignored by the converter).
    let mut md_options = Options::default();
    md_options.extension.front_matter_delimiter = Some("+++".to_owned());

    let arena = Arena::new();
    let parsed = parse_document(&arena, text, &md_options);

    // Get the frontmatter.
    let mut front_matter = String::new();
    for node in parsed.children() {
        if let NodeValue::FrontMatter(ref v) = node.data.borrow().value {
            front_matter.clone_from(v);
        };
    }

    // Clean it up.
    front_matter = front_matter.trim().to_string();
    if let Some(v) = front_matter.strip_prefix("+++") {
        front_matter = v.to_string();
    }
    if let Some(v) = front_matter.strip_suffix("+++") {
        front_matter = v.to_string();
    }
    front_matter = front_matter.trim().to_string();

    // Parse it from toml.
    match kind {
        PostKind::BlogPost => FrontMatter::BlogPost(toml::from_str(&front_matter).unwrap()),
        PostKind::Poem => FrontMatter::Poem(toml::from_str(&front_matter).unwrap()),
    }
}

fn display_tags(tags: Vec<String>, kind: PostKind) -> impl IntoView {
    if tags.is_empty() {
        return "".into_any();
    }
    let last_tag_index = tags.len() - 1;

    tags.into_iter()
        .enumerate()
        .map(|(i, tag)| {
            if i == last_tag_index {
                view! { <A href=format!("{}{tag}", kind.tags_path())>{tag}</A> }.into_any()
            } else {
                view! { <A href=format!("{}{tag}", kind.tags_path())>{tag}</A>", " }.into_any()
            }
        })
        .collect::<Vec<_>>()
        .into_any()
}
