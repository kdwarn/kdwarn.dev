use leptos::prelude::*;
use leptos_meta::*;
use leptos_router::{components::*, path};

use crate::page::{Page, ViewPage};
use crate::posts::{Post, PostKind, Posts};

pub fn shell(options: LeptosOptions) -> impl IntoView {
    // Provides context that manages stylesheets, titles, meta tags, etc.
    provide_meta_context();
    view! {
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <Link rel="shortcut icon" type_="image/png" href="/images/favicon-16.png"/>

                // injects a stylesheet into the document <head>
                // id=leptos means cargo-leptos will hot-reload this stylesheet
                <Stylesheet id="leptos" href="/pkg/kdwarn.css"/>

                // Dark and light styles, loaded in a lower priority when not prefered.
                <Link href="/dark.css" media="(prefers-color-scheme:dark)" rel="stylesheet"/>
                <Link href="/light.css" media="(prefers-color-scheme:light)" rel="stylesheet"/>

                // Verify Fosstodon account.
                <Link href="https://fosstodon.org/@kdwarn" rel="me"/>

                // Sets the document title, but changed elsewhere.
                <Title text="kdwarn"/>

                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <AutoReload options=options.clone() />
                <HydrationScripts options islands=true />
                <MetaTags/>
            </head>
            <body>
                <App/>
            </body>
        </html>
    }
}

#[component]
pub fn App() -> impl IntoView {
    view! {
        // content for this welcome page
        <Router>
            <Header>
                <Nav/>
            </Header>
            <main>
                <Routes fallback=|| NotFound()>
                    <Route path=path!("") view=|| view! { <ViewPage page=Page::Home/>} />
                    <Route path=path!("/projects") view=|| view! { <ViewPage page=Page::Projects/>} />
                    <Route path=path!("/projects/timestudy") view=|| view! {
                        <ViewPage page=Page::Timestudy/>
                    } />
                    <Route path=path!("/projects/taskfinder") view=|| view! {
                        <ViewPage page=Page::Taskfinder/>
                    } />
                    <Route path=path!("/links") view=|| view! { <ViewPage page=Page::Links/>} />
                    <Route path=path!("/feeds") view=|| view! { <ViewPage page=Page::Feeds/>} />
                    <Route path=path!("/blog") view=|| view! {
                        <Posts kind=PostKind::BlogPost/>
                    }/>
                    <Route path=path!("/blog/tags/:tag") view=|| view! {
                        <Posts kind=PostKind::BlogPost/>
                    }/>
                    <Route path=path!("/blog/:slug") view=|| view! {
                        <Post kind=PostKind::BlogPost/>
                    }/>
                    <Route path=path!("/poetry") view=|| view! {
                        <Posts kind=PostKind::Poem/>
                    }/>
                    <Route path=path!("/poetry/:slug") view=|| view! {
                        <Post kind=PostKind::Poem/>
                    }/>
                </Routes>
            </main>
        </Router>
    }
}

#[component]
fn Header(children: Children) -> impl IntoView {
    view! {
        <header>
            <p id="site-title"><A href="/">"kdwarn"</A></p>
            <p class="center">
              <A href="https://codeberg.org/kdwarn"><CodebergIcon/></A>
              <A href="https://fosstodon.org/@kdwarn"><MastodonIcon/></A>
              <A href="/feeds"><FeedIcon/></A>
            </p>
            {children()}
        </header>
    }
}

#[component]
fn Nav() -> impl IntoView {
    view! {
        <nav>
            <ul>
                <li><A href="/blog">Blog</A></li>
                <li><A href="/projects">Projects</A></li>
                <li><A href="/poetry">Poetry</A></li>
                <li><A href="/links">Links</A></li>
            </ul>
        </nav>
    }
}

// SVG taken from joinmastodon.org, and fill altered to "currentColor" to make it respond
// to color scheme changes.
#[component]
fn MastodonIcon() -> impl IntoView {
    view! {
        <svg width="74" height="79" viewBox="0 0 74 79" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><title>Mastodon</title>
        <path d="M73.7014 17.4323C72.5616 9.05152 65.1774 2.4469 56.424 1.1671C54.9472 0.950843 49.3518 0.163818 36.3901 0.163818H36.2933C23.3281 0.163818 20.5465 0.950843 19.0697 1.1671C10.56 2.41145 2.78877 8.34604 0.903306 16.826C-0.00357854 21.0022 -0.100361 25.6322 0.068112 29.8793C0.308275 35.9699 0.354874 42.0498 0.91406 48.1156C1.30064 52.1448 1.97502 56.1419 2.93215 60.0769C4.72441 67.3445 11.9795 73.3925 19.0876 75.86C26.6979 78.4332 34.8821 78.8603 42.724 77.0937C43.5866 76.8952 44.4398 76.6647 45.2833 76.4024C47.1867 75.8033 49.4199 75.1332 51.0616 73.9562C51.0841 73.9397 51.1026 73.9184 51.1156 73.8938C51.1286 73.8693 51.1359 73.8421 51.1368 73.8144V67.9366C51.1364 67.9107 51.1302 67.8852 51.1186 67.862C51.1069 67.8388 51.0902 67.8184 51.0695 67.8025C51.0489 67.7865 51.0249 67.7753 50.9994 67.7696C50.9738 67.764 50.9473 67.7641 50.9218 67.7699C45.8976 68.9569 40.7491 69.5519 35.5836 69.5425C26.694 69.5425 24.3031 65.3699 23.6184 63.6327C23.0681 62.1314 22.7186 60.5654 22.5789 58.9744C22.5775 58.9477 22.5825 58.921 22.5934 58.8965C22.6043 58.8721 22.621 58.8505 22.6419 58.8336C22.6629 58.8167 22.6876 58.8049 22.714 58.7992C22.7404 58.7934 22.7678 58.794 22.794 58.8007C27.7345 59.9796 32.799 60.5746 37.8813 60.5733C39.1036 60.5733 40.3223 60.5733 41.5447 60.5414C46.6562 60.3996 52.0437 60.1408 57.0728 59.1694C57.1983 59.1446 57.3237 59.1233 57.4313 59.0914C65.3638 57.5847 72.9128 52.8555 73.6799 40.8799C73.7086 40.4084 73.7803 35.9415 73.7803 35.4523C73.7839 33.7896 74.3216 23.6576 73.7014 17.4323ZM61.4925 47.3144H53.1514V27.107C53.1514 22.8528 51.3591 20.6832 47.7136 20.6832C43.7061 20.6832 41.6988 23.2499 41.6988 28.3194V39.3803H33.4078V28.3194C33.4078 23.2499 31.3969 20.6832 27.3894 20.6832C23.7654 20.6832 21.9552 22.8528 21.9516 27.107V47.3144H13.6176V26.4937C13.6176 22.2395 14.7157 18.8598 16.9118 16.3545C19.1772 13.8552 22.1488 12.5719 25.8373 12.5719C30.1064 12.5719 33.3325 14.1955 35.4832 17.4394L37.5587 20.8853L39.6377 17.4394C41.7884 14.1955 45.0145 12.5719 49.2765 12.5719C52.9614 12.5719 55.9329 13.8552 58.2055 16.3545C60.4017 18.8574 61.4997 22.2371 61.4997 26.4937L61.4925 47.3144Z" fill="inherit"/>
        </svg>
    }
}

// From <https://codeberg.org/Codeberg/Community/issues/976>
#[component]
fn CodebergIcon() -> impl IntoView {
    view! {
        <svg stroke="currentColor" fill="currentColor" stroke-width="0" role="img" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><title>Codeberg</title><path d="M11.955.49A12 12 0 0 0 0 12.49a12 12 0 0 0 1.832 6.373L11.838 5.928a.187.14 0 0 1 .324 0l10.006 12.935A12 12 0 0 0 24 12.49a12 12 0 0 0-12-12 12 12 0 0 0-.045 0zm.375 6.467l4.416 16.553a12 12 0 0 0 5.137-4.213z"></path></svg>
    }
}

#[component]
pub fn FeedIcon() -> impl IntoView {
    view! {
        <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" fill="currentColor"><title>Feeds</title><g><path d="M6,53.5C6,38.8,6.2,24.2,6,9.5c-0.1-3.8,1-4.7,4.7-4.6c42.2,0.3,83.6,6.4,124.3,17.6   c51.5,14.1,99.1,36.4,143.3,66.3c38.8,26.3,73.3,57.4,103.2,93.4c40.2,48.3,70.5,102.2,90.8,161.8c17.7,52.2,26.4,105.8,27.4,160.8   c0.1,3-1.1,3.7-3.8,3.7c-29.3-0.1-58.7-0.1-88,0c-3.5,0-3.6-1.8-3.6-4.4c0-20.7-1.8-41.2-5.1-61.7c-5.9-36.3-16.4-71.2-31.6-104.7   c-16.2-35.7-37-68.4-62.7-98c-26.4-30.5-56.6-56.7-90.9-78.1c-30.6-19.2-63.2-34.1-97.9-44.2c-34.1-10-68.8-15.5-104.4-15.6   c-4.4,0-5.9-1-5.8-5.8C6.3,81.8,6,67.6,6,53.5z"/><path d="M6,224.2c0-14.5,0.1-29-0.1-43.5c-0.1-3.8,1.1-4.7,4.8-4.7c38.6,0.9,76.1,7.9,112,22.2   c45.1,17.8,84.3,44.5,117.6,79.7c31.5,33.3,55,71.6,70.8,114.6c13.3,36.2,19.8,73.5,20.5,111.9c0.1,3.2-1,3.9-3.9,3.9   c-29-0.1-58-0.1-87,0c-3.6,0-4.5-1.2-4.5-4.6c-0.3-40.8-10.7-78.9-31-114.3c-19.1-33.3-44.9-60.4-77.3-81.3   c-28.5-18.3-59.6-29.2-93-33.9c-8.2-1.2-16.6-1.8-24.9-1.4c-3.4,0.1-4.2-1.1-4.1-4.3C6.1,253.8,6,239,6,224.2z"/><path d="M71.9,374.5c36.2,0,65.4,29.7,65.4,66.6c0,37.1-29.3,66.9-65.7,66.9c-36.5,0-65.7-29.8-65.6-67   C6,403.8,35,374.5,71.9,374.5z"/></g></svg>
    }
}

/// Notify user if 404.
#[component]
pub fn NotFound() -> impl IntoView {
    view! {
        <h1>"Not Found"</h1>
    }
}
