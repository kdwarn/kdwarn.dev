# Feeds

If you're interested, you can subscribe to the following feeds:
  * <a rel="external" href="/blog/feed.xml">all blog posts</a>
    * <a rel="external" href="/blog/tags/coding/feed.xml">coding blog posts</a>
    * <a rel="external" href="/blog/tags/daybook/feed.xml">daybook</a>
    * <a rel="external" href="/blog/tags/outdoors/feed.xml">outdoors blog posts</a>
  * <a rel="external" href="/poetry/feed.xml">poetry</a>
