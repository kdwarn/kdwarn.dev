# Links

Back to the future, because search engines now suck because most search engine providers are trying to inflate their stock prices by using so-called "AI" everywhere (and wasting vast amounts of energy, further fucking up the environment, in the process). 

Anyway, stuff here may get its own page if the content merits it. Not really sure how this is going to work out yet. Presence here doesn't necessarily mean I fully endorse it, but does at least mean it seems interesting and potentially worth checking out.

Contents:
  * [Outdoors](#outdoors)
  * [AI Sucks](#ai-sucks)

## Outdoors

Some good podcasts:
  * [Roots and All](https://rootsandall.co.uk/)
  * [This Old Tree](https://www.thisoldtree.show/)
  * [The Native Plant Podcast](https://www.nativeplantpodcast.com/)
  * [Cultivating Place](https://www.cultivatingplace.org/blog-1/categories/cultivating-place)
  * [Native Plants, Healthy Planet](https://bleav.com/shows/native-plants-healthy-planet/)
  * [The Urban Farm Podcast](https://www.urbanfarm.org/blog/podcast/)
  * [In Defense of Plants](https://www.indefenseofplants.com/)
  * [Plant People](https://play.prx.org/listen?uf=https%3A%2F%2Fplantpeople.podcast.nybg.org%2F)
  * [My Favorite Trees](http://mftpodcast.com/)
  * [The Plant a Trillion Trees Podcast](https://www.theplantatrilliontreespodcast.com/)

### Wildlife and Conservation

* [Homegrown National Park](https://homegrownnationalpark.org/)
* [Nature's Best Hope: A New Approach to Conservation That Starts in Your Yard](https://bookshop.org/p/books/nature-s-best-hope-a-new-approach-to-conservation-that-starts-in-your-yard-douglas-w-tallamy/8981364) by Doug Tallamy. He was interviewed on [episode 97](https://rootsandall.co.uk/podcast/episode-68-natures-best-hope-with-prof-douglas-w-tallamy/) of Roots and All.

### Gardening, including Food Forests

* Roots and All, episode 75: [Intro to Forest Gardening with Jake Rayson](https://rootsandall.co.uk/podcast/episode-46-introduction-to-forest-gardening-with-jake-rayson/)
* Roots and All, episode 183: [Food Forest in your Garden](https://rootsandall.co.uk/podcast/episode-154-food-forest-in-your-garden/)

### Forests and Trees

* Roots and All, episode 7: [Trees with Peter Thurman](https://rootsandall.co.uk/podcast/episode-7-trees/)
* Roots and All, episode 137: [Dr Glynn Percival of Bartlett Tree Experts](https://rootsandall.co.uk/podcast/episode-108-glynn-percival-of-bartlett-tree-experts/)
* [Forest Stewardship: Backyard Trees](https://extension.psu.edu/forest-stewardship-backyard-trees), article by William Elmendorf
* [Nature & Therapy](https://natureandtherapy.co.uk/). The founder was interviewed by Roots and All on episode 48: [Forest Bathing with Stefan Batorijs](https://rootsandall.co.uk/podcast/episode-26-forest-bathing-with-stefan-batorijs/)
* [Discover the Forest](https://discovertheforest.org/) - "Make the forest part of your family's story. Find a forest or park near you to get started."

### Lawns

* Roots and All, episode 100: [Tapestry Lawns with Dr Lionel Smith](https://rootsandall.co.uk/podcast/episode-71-tapestry-lawns-with-dr-lionel-smith/)
* [Roll Back Your Turf: How to reduce your lawn and choose native plants that work](https://www.nativeplanttrust.org/documents/1421/RollBackYourTurf.pdf), from the Spring/Summer 2024 issue of [*Native Plant News*](https://www.nativeplanttrust.org/about/our-magazine/)
* [How to Create a Meadow in Southeastern Pennsylvania: The Basics](https://pecpa.org/wp-content/uploads/2022/04/Water-Resources-How-to-Create-a-Meadow.pdf), by the Pennsylvania Environmental Council

### Foraging

* Roots and All, episode 155: [Wild Food with Marlow Renton](https://rootsandall.co.uk/podcast/episode-126-wild-food-with-marlow-renton/)
* Roots and All, episode 151: [The Forager’s Garden with Anna Locke](https://rootsandall.co.uk/podcast/episode-122-the-foragers-garden-with-anna-locke/)

## AI Sucks

* [Pivot to AI](https://pivot-to-ai.com/)
* Gizmodo, "[ChatGPT’s Political Views Are Shifting Right, a New Analysis Finds](https://gizmodo.com/chatgpts-political-views-are-shifting-right-a-new-analysis-finds-2000562328)", Feb 11, 2025.
* Wired, "[Thomson Reuters Wins First Major AI Copyright Case in the US](https://www.wired.com/story/thomson-reuters-ai-copyright-lawsuit/)", Feb 11, 2025.
* ArsTechnica, "[New hack uses prompt injection to corrupt Gemini’s long-term memory](https://arstechnica.com/security/2025/02/new-hack-uses-prompt-injection-to-corrupt-geminis-long-term-memory/)", Feb 11, 2025.
* 404 Media, "[Microsoft Study Finds AI Makes Human Cognition “Atrophied and Unprepared”](https://www.404media.co/microsoft-study-finds-ai-makes-human-cognition-atrophied-and-unprepared-3/)", Feb 10, 2025.
* Microsoft, "[The Impact of Generative AI on Critical Thinking: Self-Reported Reductions in Cognitive Effort and Confidence Effects From a Survey of Knowledge Workers](https://www.microsoft.com/en-us/research/publication/the-impact-of-generative-ai-on-critical-thinking-self-reported-reductions-in-cognitive-effort-and-confidence-effects-from-a-survey-of-knowledge-workers/)", April 2025.
* 404 Media, "[AI-Generated Slop Is Already In Your Public Library](https://www.404media.co/ai-generated-slop-is-already-in-your-public-library-3/)", Feb 4, 2025.
* UPI, "[Google drops promise not to use AI for weapons](https://www.upi.com/Top_News/US/2025/02/05/Google-AI-weapons/6851738775734/)", Feb 5, 2025.
* 404 Media, "[‘Things Are Going to Get Intense:’ How a Musk Ally Plans to Push AI on the Government](https://www.404media.co/things-are-going-to-get-intense-how-a-musk-ally-plans-to-push-ai-on-the-government/)", Feb 4, 2025.
* 404 Media, "[AI Company Asks Job Applicants Not to Use AI in Job Applications](https://www.404media.co/anthropic-claude-job-application-ai-assistants/)", Feb 3, 2025. 
* 404 Media "[OpenAI Furious DeepSeek Might Have Stolen All the Data OpenAI Stole From Us](https://www.404media.co/openai-furious-deepseek-might-have-stolen-all-the-data-openai-stole-from-us/)", Jan 29, 2025.
* ArsTechnica, "[AI haters build tarpits to trap and trick AI scrapers that ignore robots.txt](https://arstechnica.com/tech-policy/2025/01/ai-haters-build-tarpits-to-trap-and-trick-ai-scrapers-that-ignore-robots-txt/)", Jan 28, 2025.
* Futurism.com, "[Schools Using AI Emulation of Anne Frank That Urges Kids Not to Blame Anyone for Holocaust](https://futurism.com/the-byte/ai-anne-frank-blame-holocaust)", Jan 18, 2025.
* Tom's Guide, "[Microsoft 365 gets massive 45% price hike — and it's all to do with AI tools](https://www.tomsguide.com/computing/microsoft-365-gets-massive-45-percent-price-hike-and-its-all-to-do-with-ai-tools)", Jan 17, 2025.
* CNN, "[Apple is pulling its AI-generated notifications for news after generating fake headline](https://edition.cnn.com/2025/01/16/media/apple-ai-news-fake-headlines/index.html)", Jan 16, 2025.
* The Mirrow, "[Keir Starmer vows AI won't just lead to job losses as union shares alarm over plan](https://www.mirror.co.uk/news/politics/keir-starmer-vows-ai-wont-34472574)", Jan 13, 2025.
* 404 Media, "[CEO of AI Music Company Says People Don’t Like Making Music](https://www.404media.co/ceo-of-ai-music-company-says-people-dont-like-making-music/)", Jan 13, 2025.
* Futurism.com, "[AI Is Like Tinkerbell: It Only Works If We Believe in It](https://futurism.com/ai-tinkerbell)", Jan 11, 2025.
* TechCrunch, "[FTC orders AI accessibility startup accessiBe to pay $1M for misleading advertising](https://techcrunch.com/2025/01/03/ftc-orders-ai-accessibility-startup-accessibe-to-pay-1m-for-misleading-advertising/?guccounter=1)", Jan 3, 2025.
* CNN, "[Meta scrambles to delete its own AI accounts after backlash intensifies](https://edition.cnn.com/2025/01/03/business/meta-ai-accounts-instagram-facebook/index.html)", Jan 3, 2025.
* WBUR, "[How AI is polluting our culture](https://www.wbur.org/onpoint/2024/12/31/ai-content-humanity-neuroscience-digital)", Dec 31, 2024.
* Inequality.org, "[AI’s Energy Demands Are Fueling the Climate Crisis](https://inequality.org/article/ais-energy-demands-are-fueling-the-climate-crisis/)", Dec 16, 2024.
* Futurism.com, "[Schools Using AI to Send Police to Students' Homes](https://futurism.com/the-byte/schools-ai-send-police-homes)", Dec 14, 2024.
* Bloomberg, "[OpenAI CEO, Perplexity Give $1 Million to Trump’s Inaugural Fund](https://www.bloomberg.com/news/articles/2024-12-13/openai-ceo-perplexity-give-1-million-to-trump-s-inaugural-fund)", Dec 13, 2024.
* TechCrunch, "[UnitedHealth’s Optum left an AI chatbot, used by employees to ask questions about claims, exposed to the internet](https://techcrunch.com/2024/12/13/unitedhealthcares-optum-left-an-ai-chatbot-used-by-employees-to-ask-questions-about-claims-exposed-to-the-internet/)", Dec 13, 2024.
* BBC, "[BBC complains to Apple over misleading shooting headline](https://www.bbc.com/news/articles/cd0elzk24dno)", Dec 13, 2024. 
* Mercury News, "[OpenAI whistleblower found dead in San Francisco apartment](https://www.mercurynews.com/2024/12/13/openai-whistleblower-found-dead-in-san-francisco-apartment/)", Dec 13, 2024.
* Reuters, "[Big Oil eyes powering Big Tech's data center demand](https://www.reuters.com/business/energy/chevron-working-supply-power-data-centers-executive-says-2024-12-11/)", Dec 12, 2024.
* The Register, "[Open source maintainers are drowning in junk bug reports written by AI](https://www.theregister.com/2024/12/10/ai_slop_bug_reports/)", Dec 10, 2024.
* Gizmodo, "[AI Firm’s ‘Stop Hiring Humans’ Billboard Campaign Sparks Outrage](https://gizmodo.com/ai-firms-stop-hiring-humans-billboard-campaign-sparks-outrage-2000536368)", Dec 10, 2024.
* Ars Technica, "[Chatbots urged teen to self-harm, suggested murdering parents, lawsuit says](https://arstechnica.com/tech-policy/2024/12/chatbots-urged-teen-to-self-harm-suggested-murdering-parents-lawsuit-says/)", Dec 10, 2024.
* The Virginia Mercury, "[Under pressure from the SCC, Dominion reveals the true cost of data centers](https://virginiamercury.com/2024/11/26/under-pressure-from-the-scc-dominion-reveals-the-true-cost-of-data-centers/)", Nov 26, 2024. 
* Baldur Bjarnason, *The Intelligence Illusion (Second Edition): Why generative models are bad for business*, 2nd ed. 2024. See [this post](https://www.baldurbjarnason.com/2024/intelligence-illusion-2nd-ed-launch-black-friday/) for more about the book.
* 404 Media, "[AI-Powered Buzzfeed Ads Suggest You Buy Hat of Man Who Died by Suicide](https://www.404media.co/ai-powered-buzzfeed-ads-suggest-you-buy-hat-of-man-who-died-by-suicide/)", Nov 18, 2024.
* In These Times, "[A Microsoft AI Data Center Saps Water From A Small Mexican Town](https://inthesetimes.com/article/microsoft-ai-data-center-water-climate)", Nov 14, 2024.
* IEEE Spectrum, "[It's Surprisingly Easy to Jailbreak LLM-Driven Robots](https://spectrum.ieee.org/jailbreak-llm)", Nov 11, 2024.
* Ars Technica, "[How The New York Times is using generative AI as a reporting tool](https://arstechnica.com/ai/2024/10/the-new-york-times-shows-how-ai-can-aid-reporters-without-replacing-them/)", Oct 29, 2024.
* ABC News, "[Florida mother files lawsuit against Character.ai over son's death by suicide](https://abcnews.go.com/Nightline/video/florida-mother-files-lawsuit-characterai-sons-death-suicide-115095727)", Oct 24, 2024.
* New York Times, "[Former OpenAI Researcher Says the Company Broke Copyright Law](https://www.nytimes.com/2024/10/23/technology/openai-copyright-law.html)", Oct 23, 2024.
* Washington Post, "[Three Mile Island owner seeks taxpayer backing for Microsoft AI deal](https://www.washingtonpost.com/business/2024/10/03/nuclear-microsoft-ai-constellation/)", Oct 3, 2024.
* Bulletin of the Atomic Scientists, "[A new military-industrial complex: How tech bros are hyping AI’s role in war](https://thebulletin.org/2024/10/a-new-military-industrial-complex-how-tech-bros-are-hyping-ais-role-in-war/)", Oct 7, 2024.
* The Times. "[‘Thirsty’ ChatGPT uses four times more water than previously thought](https://www.thetimes.com/uk/technology-uk/article/thirsty-chatgpt-uses-four-times-more-water-than-previously-thought-bc0pqswdr)", Oct 4, 2024.
* Ars Technica, "[Omnipresent AI cameras will ensure good behavior, says Larry Ellison](https://arstechnica.com/information-technology/2024/09/omnipresent-ai-cameras-will-ensure-good-behavior-says-larry-ellison/)", Sep 16, 2024.
* The Guardian, "[Data center emissions probably 662% higher than big tech claims. Can it keep up the ruse?](https://www.theguardian.com/technology/2024/sep/15/data-center-gas-emissions-tech)", Sep 15, 2024.
* The Atlantic, "[Microsoft’s Hypocrisy on AI](https://www.theatlantic.com/technology/archive/2024/09/microsoft-ai-oil-contracts/679804/?gift=lhL3dXSYCcu9vqTqEbg0OHfJiu_TRdq079IHN4QaSAE)", Sep 13, 2024.
