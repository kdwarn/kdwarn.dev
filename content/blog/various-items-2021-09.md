+++
title = "Various items"
date = "2021-09-14"
tags=["daybook", "bash", "database", "python", "ansible"]
+++

Bash: I think I already knew this, or at least had a general feeling that it was true, but now I read explicitly (in Miell's *Learn Bash the Hard Way*) that wildcards are not the same as regular expressions.

Web apps/postgres: connection pools could have a significant effect on performance when there's a fair amount of traffic.

Python: you can include on requirements file inside another. Useful for a base/production file and then a separate dev reqs file.

Ansible: set_fact allows you to create variables at runtime (most variables are created at beginning of play).

