+++
title = "Raised bed deer protection finished"
date = "2024-05-28"
tags = ["outdoors", "gardening", "vegetables", "wildlife"]
+++

Around 8pm, I finally finished putting up the fencing to protect the raised bed from deer. For now, they are the main issue, trimming off the tops of some of the vegetables I planted. Later, I'll probably have an issue with chipmunks and squirrels since they can still easily get in. My first step for dealing with that is to plant some rosemary at the obvious entries - we'll see if that works. (And if not, oh well, it's the first year so I'm kind of expecting the worst.)

Anyway, after ten hours over the past month, it's complete. Two of the four sides of the fencing act as doors and seem to be functioning well. We'll see how long they hold up, but it's easy enough (though a little too difficult and awkward) to open and close them. I nailed the fencing to the structure and then to 1x1s that then connect to the frame by a hook. It's probably not the final version, but it works for now.

<br><br>
Here it is on May 11, when I'd completed the frame and put up the first portion of fencing:

<img src="/images/outdoors/2024-05-11-raised-bed.jpg">

<br><br>
One of the foxes came to investigate right when I was wrapping up on May 28:

<img src="/images/outdoors/2024-05-28-fox.jpg">

<br><br>
A view inside the garden a few days after completing the fencing, on June 1:

<img src="/images/outdoors/2024-06-01-raised-bed.jpg">
