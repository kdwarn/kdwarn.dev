+++
title = "Daffodil bulbs planted"
date = "2024-02-25"
tags = ["outdoors", "gardening"]
+++

I had wanted to plant these in the fall, but ran out of time to do it. So in they went at the end of February. In total, I probably planted 50+ bulbs. They are along the steps up to the terrace, then in a cluster at the top, and finally between two of the pillars.

They all seemed to do well. 
