+++
title = "rsync with --update"
date = "2025-02-08"
tags = ["daybook", "terminal"]
+++

Calling `rsync` with the `--update` flag will "skip files that are newer on the receiver", according to the man page. That is, it will only copy the file over if it is newer than the destination file. `scp` can't do this, and so it's a nice convenience even if `rsync` needs to be installed. I discovered this as I am trying to rely more on [`just`](https://just.systems/) and the shell than Ansible, which is good but also kind of gets you out of practice.
