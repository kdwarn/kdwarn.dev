+++
title = "Joined Homegrown National Park"
date = "2025-01-11"
tags=["outdoors", "gardening"]
+++

Not much else to say about it, really. I've only just joined and put myself on the [map](https://map.homegrownnationalpark.org/). It would be great if the vision in the [book](https://www.hachettebookgroup.com/titles/douglas-w-tallamy/natures-best-hope/9781604699005/?lens=timber-press) were put into practice. Whether or not that is being done by whatever effort is being put into it with this website is a question I have. So far it seems a little fancy up front but superficial. 
