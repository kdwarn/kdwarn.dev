+++
title = "Summer 2024"
date = "2024-09-22"
tags = ["outdoors"]
+++

I made more progress on desodding and mulching - slightly extended the area by the mailbox, created area on the hillside by the swingset, and created the area from the arborvitaes almost to the maple on the western line. Got and assembled a picnic table. All kinds of weeding and removing vines. Got lumber to make small raised bed dedicated to garlic. Finished removing excess soil around house. Transplanted ferns. Got a decent amount of things from vegetable garden - at least a few quarts of green beans, a pint of tomatoes so far, lots of squash, beets, a couple carrots, a bunch of cucumbers. Threw in some potato eyes a few weeks ago and now have several good plants - we'll see what comes of them.

Tasks completed:
<ul>
  <li>plant ferns (that got dug up with vines) on western tree line</li>
  <li>use D's truck to get mulch</li>
  <li>use D's truck to get picnic table for terrace</li>
  <li>move wood from driveway to firewood pile</li>
  <li>expand front mulched area - cover with thick paper, throw mulch over it</li>
  <li>assemble picnic table </li>
  <li>cut more boards for compost bins</li>
  <li>move giant stone from under false holly</li>
  <li>bring veggies to SE due:2024-07-17 (2024-07-17)</li>
  <li>mulch around blackberries (2024-07-18)</li>
  <li>remove vines from false holly in back (again) (2024-07-18)</li>
  <li>sand picnic table (2024-07-18)</li>
  <li>stain picnic table (2024-07-20)</li>
  <li>recage two blueberries (2024-07-21)</li>
  <li>weed left blueberry (2024-07-21)</li>
  <li>try to propagate dogwood (2024-07-21)</li>
  <li>mulch around raspberries (2024-07-21)</li>
  <li>weed overgrown blueberry (2024-07-30)</li>
  <li>open up the tulip tree cage so the branches have room to spread out (2024-08-07)</li>
  <li>weed/mulch around Japanese maple in front (2024-08-07)</li>
  <li>weed hickories (2024-08-24)</li>
  <li>finish removing excess soil from in front of dining room - use this for raised bed for garlic completed (2024-08-25)</li>
  <li>record the specific vegetables and herbs I planted this year (2024-08-25)</li>
  <li>desod and mulch between trees on western line, from gazebo past the oak (2024-08-25)</li>
  <li>weed/remulch around blackberries (2024-09-02)</li>
  <li>buy lumber for raised bed for garlic (2x8x8) (2024-09-16)</li>
</ul>
