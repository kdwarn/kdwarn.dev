+++
title = "Braces"
date = "2021-10-02"
tags=["daybook", "rust", "bash"]
+++

Rust and Bash have a very similar construct for iterating over ranges: x..y. Bash uses it like `for i in {1..9}` while Rust doesn't use the braces (though seems like parentheses may be used - but not required).

I spent some time reworking my bash note on parameter expansion (actually calling the value of a parameter by using the `$` symbol), and think I have further strengthened my understanding of it, using doubles quotes in conjunction with it, and using braces in conjunction with it.

One important reason to use braces around a parameter's name (but after the $, like this: `"${somevar}"`), is that you can make a default value, either for immediate expansion or to assign the default value to the parameter. Immediate expansion (won't be used again if you reuse the parameter): `${somevar:-this}"`. Assignment (assigned here, will be used as value of the parameter in subsequent uses): `"${somevar:=this}"`.

