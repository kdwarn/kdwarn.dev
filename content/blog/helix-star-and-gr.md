+++
title = "Helix * and gr"
date = "2024-06-14"
tags=["daybook", "helix"]
+++

`*` and `gr`

I learned two useful things in Helix today:
  1. You can use `*` then `n` to do a search for the current selection. Say you're in the middle of some word. Use `miw` to select that word, `*` to put it into the default register (`/`), and then `n` to find the next occurrence of it.
  2. `gr` is "go to reference". It pulls up a list of all the occurrences of whatever you're referring to. So, as in above you can select a word with `miw` and then type `gr` to open the picker where you can navigate through the list, previewing it. Pressing enter on one will take you to that position in the code.
