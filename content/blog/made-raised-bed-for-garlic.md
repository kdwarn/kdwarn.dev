+++
title = "Made raised bed for garlic"
date = "2024-09-22"
tags = ["outdoors", "gardening", "vegetables", "garlic"]
+++

I planted garlic last year in October in the raised bed, and nothing touched it the entire time the raised bed was unfenced, so I decided to make a separate, unfenced bed for it. Another simple project: two 8-feet-long 2x8s, cut in half, and screwed together. The compost pile contributed some rich new dirt, and dirt moved from elsewhere, grass clippings, and excess mulch made up the rest. In a couple weeks, I'll plant some of the cloves from the garlic harvested this summer.

<img src="/images/outdoors/2024-09-22-raised-bed-for-garlic.jpg">
