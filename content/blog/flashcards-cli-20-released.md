+++
title = "Flashcards CLI 2.0 Released"
date = "2021-03-10"
tags=["coding", "python", "cli"]
+++

A little less than a year ago I was looking for a flashcards command-line interface, mainly so I could help myself memorize various Vim commands. Though I'd previously used [AnkiWeb](https://ankiweb.net/) for studying, I wanted a CLI because I spend much of my time in the terminal and I wanted to be able to quickly add a new "card" and then jump back to whatever I was doing.

I primarily program in Python, so I tried out two or three different CLIs that I found on PyPI. One was broken on installation (at least with Python 3), and someone else had already raised an issue on GitHub a few months before with the same problem I had. It looked like a very easy fix so I forked the project and submitted a pull request. I didn't get a response, but I liked the interface so I started to use [my version of it](https://codeberg.org/kdwarn/flashcards), and meanwhile poked around the code more. I found some areas that could be simplified, and then more and more kept appearing. Soon I was gutting most of the object-oriented code in favor of a more structural approach. Between that, switching from unittest to pytest, adding more features, and moving from file-based storage to a database, there's not a whole lot of the code I started with left.

Going from file-based storage to an sqlite database was a breaking change (and what necessitated version 2.0), but I think it was a good decision. It was prompted by sqlite repeatedly [popping up](https://news.ycombinator.com/item?id=26151302) [lately](https://twitter.com/search?q=sqlite%20(from%3Asimonw)), but also by my desire to make "reversible" cards, so that either the "question" or "answer" could appear before the other during a study session, rather than always question-then-answer. That latter part would have required some structural change within the files, and I felt like they were already cumbersome to work with. Switching to a database would not only allow me to further simplify the codebase, but also make it easier to continue to evolve features.

I doubt that anyone aside from myself had been using my version of flashcards 1.x, but just in case — and because I needed to do it for myself anyway — [v2.0](https://codeberg.org/kdwarn/flashcards/releases/tag/v2.0) (but not >= v2.1) includes a command to convert to the new format. I still have some plans for improvements, but I somewhat doubt that there will ever be a version 3.0.

Try it out if you're looking to memorize some things and like the command line!
