+++
title = "Second Chestnut Planted"
date = "2024-12-10"
tags = ["outdoors", "trees"]
+++

I planted the second chestnut given to me by MH, to go along with the [first one](/blog/first-chestnut-planted) planted two years ago, in December 2022. Just as the first one, this one is also  from the [American Chestnut Foundation](https://tacf.org/) - their "Improved American Chestnut Seeds with Intermediate Blight Resistance" stock. These are the only ones they offer east of the Mississippi, due to the [chestnut blight](https://en.wikipedia.org/wiki/Chestnut_blight). From the ACF's website:

> These hybrid seeds have some level of Chinese chestnut ancestry to confer blight resistance. They will tolerate blight infection more effectively than an American chestnut, but not as well as a Chinese chestnut. In general, blight will grow rapidly on an American chestnut, more slowly on these hybrids, and do minimal damage to a Chinese chestnut. At this time there is no 100% blight-resistant American chestnut seed or seedling and there is no guarantee that any seed will grow free from blight. Scientific efforts toward this goal are part of TACF’s mission.

Mine's ID is W8-23-18XOP. I briefly tried to figure out what the numbers mean with no luck, though I assume the "23" is for the year it was started, since the previous one I planted in 2022 had "20" in that position.

It's on the front line between the maple that was already here and [the American hornbeam I planted](/blog/american-hornbeam-planted) earlier in the year, and diagonal to the first chestnut, maybe 20 feet away.

<figure>
<img src="/images/outdoors/2024-12-10-chestnut.jpg"/>
  <figcaption>About 20" chestnut sapling planted in yard scattered with leaves dropped in fall. In back are the bottoms of two other recent plantings: an American hornbeam in the middle and an oak on the right.</figcaption>
</figure>

<figure>
<img src="/images/outdoors/2024-12-10-chestnut-id.jpg"/>
  <figcaption>The ID tag of the chestnut.</figcaption>
</figure>
