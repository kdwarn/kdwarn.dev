+++
title = "The Secret Life of Programs"
date = "2021-09-21"
tags=["daybook"]
+++

I started reading the book, *The Secret Life of Programs, Understand Computers - Craft Better Code*, by Jonathan E. Steinhart. So far it seems like what I've been after in terms of a more general book on computer science and programming. And the author's got a sense of humor, which is nice. I think it has convinced me to really try to focus on the lower level stuff, much more so than the first couple of chapters of the first volume of Randall Hyde's *Write Great Code* series.

One thing I learned - or at least deepened my understanding of - is what "system programming" is. He describes it as: "systems programming is at the bottom of the software hierarchy. It's similar to infrastructure [...]. Being a good programmer always matters, but it matters more if you're a system programmer, because others rely on your infrastructure." Says you need to learn about application programming and computer hardware to be a system programmer.

There was also a decent discussion of the differences between coding, programming, engineering, and computer science.

Something that also resonated with me was this statement: "But the strange thing about computer programming is that unlike in medicine, in programming you can become a specialist without ever being a generalist." That's kind of where I'm at/heading (web development at the backend) unless I purposefully steer towards a more general level, which I want to do, and I think I'll be a better overall programmer for it.

