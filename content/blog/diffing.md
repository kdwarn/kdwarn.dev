+++
title = "Diffing"
date = "2024-04-15"
tags=["daybook", "vcs"]
+++

There are (at least) two ways to view the diff of both an untracked file and tracked-but-changed/not-staged files in git.

1. Add the untracked file, run `git diff HEAD`. If it turns out you don't want that file added, just follow the command shown on with `git status`: `git restore --staged <file>`. 

2. Use the `-N` (`--intent-to-add`) flag in `git add`, which "record[s] only the fact that the path will be added later" (from `git add --help`). As the help continues, "This is useful for, among other things, showing the unstaged content of such files with `git diff` and committing them with `git commit -a`." 

Not sure which way I'll settle on, but I'm glad I finally looked into it. I generally do the first version, but only because I didn't know any other way. And before I end up doing that, I usually think there is some simple flag I'm missing with `git diff` that'll do what I want, before realizing - once again - that there's not.
