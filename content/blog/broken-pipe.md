+++
title = "Broken Pipe"
date = "2023-05-11"
tags=["daybook", "rust"]
+++

I went down a little rabbit hole after getting a "broken pipe" error when trying to pipe the output of [`ts-cli`](https://codeberg.org/kdwarn/ts-cli) to another program (`head`/`tail`). The short version of what I've learned is that repeatedly calling `println!` is not very performant and can cause this error. So what I'm going to do is stick all output into a String, locking a `BufWriter` on `io::stdout`, and writing to it with `writeln!` (and calling `.ok()` on it to ignore any errors). (I also tried using `writeln!` every time something needed to be printed, but this slows this down significantly. So both are needed - a single string and the lock/writeln! once. This is now done on the Summary command and pipes are working.

