+++
title = "Ashe's magnolia and bald cypress planted"
date = "2023-10-28"
tags = ["outdoors", "trees"]
+++

I got both of the trees from the township's fall giveaway. They came with good fencing too. The magnolia went in easily, less so with the bald cypress because of rocks. In all, about 3 hours work to put them in the ground. Not bad at all for the benefits of trees. Glad to have some less common species, and that they are native. 
