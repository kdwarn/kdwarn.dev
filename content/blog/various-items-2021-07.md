+++
title = "Various items"
date = "2021-07-12"
tags=["daybook", "bash", "python", "ansible"]
+++

Bash: easy way to test out conditions is to do it right on the command line, and then echo out the result of the last command, e.g.:

```sh
[[ 001 == 1 ]]
echo $?
```

That will result in "1" being printed to the terminal, since `==` does string evaluation.

Python: `pypandoc` (wrapper around program `pandoc`) is super easy to use to convert strings and files from one format to another. 

Also, not something I learned but a good practice I've developed: anytime I want to install some new program, I do it via the Ansible playbook I set up for my computer. This way, I get more experience with ansible, and I can reproduce my computer if it dies unexpectedly or I get a new one or even if I just switch distros.

