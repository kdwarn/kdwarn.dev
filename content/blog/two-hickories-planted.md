+++
title = "Two hickories planted"
date = "2024-04-24"
tags = ["outdoors", "trees"]
+++

I haven't taken pictures of these yet, but I planted the two hickories gifted by MH. They are in the shared area beyond the rock wall, bordering JM & CM.
