+++
title = "Generic and traits"
date = "2022-04-18"
tags=["daybook", "rust"]
+++

I finally have a decent understanding of generics and traits. Something clicked when I was reading about them in Ch 2 of [*Programming Rust*](https://www.oreilly.com/library/view/programming-rust-2nd/9781492052586/). I imagine that I came across similar descriptions of these before, but it really helped this time around having some more experience with the language. I don't think I knew that generic types were called "type parameters" when you put them in function signatures. So, just having a term for that helps. That's `fn<T>(t: T) {}`. And you can specify not just any type, but a type that implements a specific trait: `fn<T: FromStr>(t: T) {}`, which can be read as "For any type that implements the FromStr trait." When writing this up in my notes, I also came across returning a type that implements at trait, which makes more sense now, although I just came up with a few questions to dig deeper into the whole subject that I need to look into sometime.
