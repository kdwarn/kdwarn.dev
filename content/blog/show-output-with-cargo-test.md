+++
title = "--show-output with cargo test"
date = "2024-02-02"
tags=["daybook", "rust", "testing"]
+++

`cargo test` does not display any standard output from successful tests, just that the results were successful. To get around this and see the output of `dbg!` or `println!` statements, I usually make a false assertion so that a test fails and these get shown. However, an easier way is to use the `--show-output` flag: `cargo test -- --show-output`, as mentioned in [the Book](https://doc.rust-lang.org/book/ch11-02-running-tests.html#showing-function-output). The [`cargo test` section](https://doc.rust-lang.org/cargo/commands/cargo-test.html) of the Cargo Book does not list this (for some reason unclear to me), but instead `cargo test -- --nocapture`, which has the same effect. I'm pretty sure I've come across `--nocapture` before, but I think `--show-output` will be easier to remember. Well, maybe. I've definitely read the book and forgot about `--show-output`. Noting here should help.
