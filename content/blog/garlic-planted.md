+++
title = "Garlic planted"
date = "2023-10-01"
tags = ["outdoors", "gardening", "vegetables", "garlic"]
+++

I picked up a bag of California Giant garlic and planted it in the raised bed - first thing to go in. Here it is moments before I started pushing the cloves into the dirt:

<img src="/images/outdoors/2023-10-01-garlic.jpg">

The timing seems to have worked out well. My mom's advice was to plant during the waning moon (maybe specifically during October, I'm not sure), which was advice they heard from "coach" (Harry Pinge I believe). I think that's what I did, though at the same time I don't think it matters, though I'm open to there being some reason for this existing. I imagine just timing, and so "October 1 or thereabouts" will do for me in the future.
