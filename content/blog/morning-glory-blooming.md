+++
title = "Morning glory blooming"
date = "2024-06-12"
tags = ["outdoors", "gardening"]
+++

I've never had a morning glory before, and when I was picking out seeds for the vegetable garden earlier this spring, either I or my daughter thought it looked pretty, so we picked a packet up. I planted all the seeds - maybe 20 to 30 - and just a single one came up. It grew inside for awhile, and then I planted it at the edge of the raised bed a couple weeks ago. I hadn't noticed it even starting to form a flower bud, and then today there was this glorious flower:

<img src="/images/outdoors/2024-06-12-morning-glory.jpg">
