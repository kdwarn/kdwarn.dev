+++
title = "White fringetree planted"
date = "2024-10-27"
tags = ["outdoors", "trees"]
+++

I planted a [white fringetree](https://en.wikipedia.org/wiki/Chionanthus_virginicus), courtesy of the shade tree commission. Top of the steps to the terrace, on the left. Like all of the trees I've planted, I am interested in seeing how it turns out over the years. This one was placed to add a bit of shade to the front of terrace before the vegetable garden. With some shrubs I want to put in front of it, on the hillside next to where the foxgloves were planted, it's the start of a section of taller things that will provide a different habitat along with screening.

<figure>
<img src="/images/outdoors/2024-10-27-white-fringetree.jpg"/>
  <figcaption>White fringetree, maybe four feet tall, freshly planted in the ground on the edge of a grass lawn, with caging around it to protect it from deer.</figcaption>
</figure>
