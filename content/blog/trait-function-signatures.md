+++
title = "Trait function signatures"
date = "2022-01-07"
tags=["daybook", "rust"]
+++

I thought when you implemented a trait, the function had to have the exact same signature as the trait, but apparently you can (at least) change the parameters from being immutable to mutable. This is from [rustlings/traits/traits2](https://github.com/rust-lang/rustlings/blob/main/exercises/15_traits/traits2.rs), the instructions of which said to implement `AppendBar` on a vector of strings, such that you appended "Bar" to it:

```rust
trait AppendBar {
    fn append_bar(self) -> Self;
}

impl AppendBar for Vec<String> {
    fn append_bar(mut self) -> Self {
        self.push("Bar".to_string());
        self
    }
}
```

