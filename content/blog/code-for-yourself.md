+++
title = "Code for Yourself"
date = "2024-08-25"
tags=["daybook"]
+++

Writers have "write for yourself, not your audience" and probably a bunch of variations and maybe there's the same thing in other creative endeavors. I want to try to apply "code for yourself" when thinking about new side projects to work on. Sometimes I'll get into a "what would be a useful project?" mode of thinking when I want to do something new, and it's not productive because I just end of wasting time trying to thinking of possibilties and write nothing. I'll end up having more fun and writing more code when I just scratch my own itch. 
