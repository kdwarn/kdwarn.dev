+++
title = "Holly planted"
date = "2024-03-16"
tags = ["outdoors", "trees"]
+++

I've been carting around a small holly in a large container between the moves the last few years, and now it has found its permanent home: on the other side of the gazebo. 
