+++
title = "cal"
date = "2023-12-05"
tags=["daybook", "terminal"]
+++

`cal` is a useful command; I've often used it to get a visualization of where we're at in the week or month. Since I'm often in the terminal or a couple keystrokes away from one, it's the quickest way to do this. However, just `cal` doesn't highlight the day, which would be useful. A quick web search revealed that its partner program `ncal`, does this with `ncal -b`.

