+++
title = "Raised bed planted"
date = "2024-04-21"
tags = ["outdoors", "gardening", "vegetables"]
+++

Raised bed was planted: two types of beets, carrots, soybeans, fava beans, green and yellow squash, a pumpkin or two, peas, french filet beans. Too much for the space, but I figured some of it wouldn't make it and if everything did I'd just move some things somewhere else. Oh, and the garlic on the left that had been planted in the fall.

<img src="/images/outdoors/2024-04-21-raised-bed-planted.jpg"/>
