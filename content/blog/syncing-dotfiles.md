+++
title = "Syncing Dotfiles"
date = "2022-05-09"
tags=["coding", "vcs", "bash"]
+++

For a while I kept some configuration files in Dropbox so I could get to them outside my home computer. I wrote a simple bash script that would move them from their normal locations to a directory in my local Dropbox, and then set up a cronjob to run that script every day. That was ok, but they weren't easily accessible publicly. Or at least not in the way many people share their dotfiles, which is to just have a repo for them.

So I decided to move them from Dropbox to <strike>GitHub</strike> Codeberg, which presented a small challenge — how to do the commit and push once I collected all the files into a git repository? Here's the simplified bit of bash for that:

```sh
git_status=$(git status)

if [[ $git_status != *"nothing to commit"* ]]; then
  git add "*" && git commit -am "Update" && git push
fi
```

If the stdout of running `git status` doesn't contain "nothing to commit", then it adds all files in the repo, commits with the message "Update", and pushes it. That's not a very meaningful commit message — especially not as the *only* message in the history after the initial set up — but I'm not particularly concerned with that and more with having the files always up-to-date and accessible.

Another small challenge was with cron. I didn't want to run the script repeatedly all day, but if I just ran it once a day there was a chance my computer wouldn't be on at the time and so the cronjob wouldn't run. [Anacron](https://sourceforge.net/projects/anacron/) to the rescue! Anacron will run jobs on a regular basis like cron, except that it is aware of the last time jobs ran and will run them again if they haven't run within the specified interval. Anacron isn't installed on Linux distos by default (or at least not Debian and its derivatives), but it's a simple `sudo apt install anacron` to install it. By default, anacron's configuration file is location at /etc/anacrontab and it tracks jobs run at /var/spool/anacron. I wanted these to be in my user space, so I created those directories/files under ~/.anacron. Here is the part of the config file (~/.anacron/etc/anacrontab) related to this project:

```sh
1 3 manage_dotfiles ~/coding/dotfiles/manage_dotfiles > /dev/null
```

There are two other pieces to this. The first is including this in my ~.profile file, so that anacron runs on startup:

```sh
anacron -t "$HOME/.anacron/etc/anacrontab" -S "$HOME/.anacron/var/spool/anacron"
```

And the second is a regular cronjob that will run anacron every hour (which causes anacron to check if any jobs need to be run, and run them if so):

```
0 * * * * anacron -t "$HOME/.anacron/etc/anacrontab" -S "$HOME/.anacron/var/spool/anacron"
```

That's pretty much it. [Here](https://codeberg.org/kdwarn/dotfiles/)'s the link to the repo, which includes the full `manage_dotfiles` bash script.
