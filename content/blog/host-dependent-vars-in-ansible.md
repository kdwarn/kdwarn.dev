+++
title = "Host-dependent variables in Ansible"
date = "2021-07-24"
tags=["daybook", "ansible"]
+++

Ansible: I don't know if I learned this or re-learned this, but it's quite easy to set up host-dependent variables.

Here's one example of a file structure:

```
project/
 |--inventories/
    |--local/
    | |--host_vars/
    |   |--[name of host].yaml <-- where the variables are for this host
    |--production/
      |--host_vars/
        |--[name of host].yaml <-- where the variables are for this host
```

"group_vars" directories (at same level as "host_vars") would also work on the group level, if you have groups of hosts defined.

