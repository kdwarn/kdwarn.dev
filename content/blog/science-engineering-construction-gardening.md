+++
title = "Science, Engineering, Construction, Gardening?"
date = "2024-01-08"
tags=["daybook"]
+++

I've just been reviewing notes I had taken two years ago, within a month of each other, on two different sources. I'm not sure if I thought about it much then, if at all, but I thought they were interesting reading together today.

The first is from Brian Harvey's first lecture of [Computer Science 61A UC Berkeley in 2011](https://archive.org/details/ucberkeley-webcast-PL3E89002AA9B9879E?sort=titleSorter). In it, he says, "Computer science is a bad name for what we do. It's not science and it's not about computers. Scientists ask questions about how the world works. For the most part, what we do is more like engineering - we build stuff. That's not entirely true, because of theoretical computer science, but mostly it's engineering. And it's not about computers - that's electrical engineering. We build software. Our field should be called software engineering."

A month prior, I had quoted a sentence from David Thomas and Andrew Hunt's [*The Pragmatic Programmer*](https://en.wikipedia.org/wiki/The_Pragmatic_Programmer), noting that it was a good point: "Unfortunately, the most common metaphor for software development is building construction. Rather than construction, software is more like gardening — it is more organic than concrete." 

At first the two quotes seem to be in opposition, but I think the broader context from Thomas and Hunt shows they aren't that different, but they are starting from different positions:

> Unfortunately, the most common metaphor for software development is
building construction. Bertrand Meyer’s classic work *Object-Oriented Software
Construction* uses the term “Software Construction,” and even your
humble authors edited the Software Construction column for IEEE Software
in the early 2000s.
>
> But using construction as the guiding metaphor implies the following steps:
> 1. An architect draws up blueprints.
> 2. Contractors dig the foundation, build the superstructure, wire and plumb,
and apply finishing touches.
> 3. The tenants move in and live happily ever after, calling building maintenance to fix any problems.
>
> Well, software doesn’t quite work that way. Rather than construction, software is more like gardening — it is more organic than concrete. You plant many things in a garden according to an initial plan and conditions. Some thrive, others are destined to end up as compost. You may move plantings relative to each other to take advantage of the interplay of light and shadow, wind and rain. Overgrown plants get split or pruned, and colors that clash may get moved to more aesthetically pleasing locations. You pull weeds, and you fertilize plantings that are in need of some extra help. You constantly monitor the health of the garden, and make adjustments (to the soil, the plants, the layout) as needed. 
>
> Business people are comfortable with the metaphor of building construction: it is more scientific than gardening, it’s repeatable, there’s a rigid reporting hierarchy for management, and so on. But we’re not building skyscrapers — we aren’t as constrained by the boundaries of physics and the real world. 
>
> The gardening metaphor is much closer to the realities of software development. Perhaps a certain routine has grown too large, or is trying to accomplish too much — it needs to be split into two. Things that don’t work out as planned need to be weeded or pruned. 

I don't know that Harvey would disagree with this much; it's an extension of what he said, in a way.
