+++
title = "False hollies planted"
date = "2024-03-31"
tags = ["outdoors", "trees"]
+++

I thought I was planting hollies, but since they were planted, I discovered they actually are not. I took these from the large bush at the back property line of the house, where a branch had dipped down to the ground and layered itself. I was able to pull out three separate trunks from this, between two and three feet tall. Planting was easy, and I thought I was getting a good start to the holly hedge I'm attempting to start along the road. 

The reason I thought these were hollies was because the leaves near the bottom of the tree very much looked like holly leaves - dark green, thick, jagged edges. I remember reading some time ago that holly leaves will lose this jaggedness towards the top of the tree, and I assumed that was the reason that most of the leaves of the tree were ovate. However, once the fruit developed on the parent tree, it was obvious it wasn't a holly. Instead of a small red berry, there is a larger dark blue/purple ovoid drupe (which something enjoys eating - been finding the seeds on the playset, on the raised bed frame, and on the top of the compost bin). My first attempt at identifying it came up with Phillyrea latifolia (via the PlantNet app), but now I think it is Osmanthus heterophyllus, aka "false holly" according to [Wikipedia](https://en.wikipedia.org/wiki/Osmanthus_heterophyllus). (Another good source on it is from [Trees and Shrubs Online](https://www.treesandshrubsonline.org/articles/osmanthus/osmanthus-heterophyllus/), a publication of the International Dendrology Society, which I did not know existed until now.)

<img src="/images/outdoors/2024-05-29-false-holly-fruit.jpg">
