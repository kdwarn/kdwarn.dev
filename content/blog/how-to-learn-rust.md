+++
title = "How to Learn Rust"
date = "2023-01-21"
tags=["coding", "rust"]
+++

It's been somewhere around two years since I first began to learn and love Rust. In that time, I've read a lot of things about the programming language. Here are a few resources I recommend to someone who wants to learn it as well.

Read this first: [How Not to Learn Rust](https://dystroy.org/blog/how-not-to-learn-rust/)

Rust has [its own list of learning resources](https://www.rust-lang.org/learn), and I think they should be the starting point for anyone:
  * [The Book](https://doc.rust-lang.org/stable/book/)
  * [Rustlings](https://github.com/rust-lang/rustlings/)
  * [Rust by Example](https://doc.rust-lang.org/stable/rust-by-example/)

However, I'd say that Rust by Example was the least useful to me, perhaps because I did the book and Rustlings first. In any case, I recommend doing something like this: read the book and do the Rustlings course at the same time, then work on some small project, check out Rust by Example, then read the book and do Rustlings again. I think I'm on my fourth run of Rustlings and each time I notice that my skills are improved, but there are also still some things that I have to seek help for.

Also, note that there's now [an interactive version of the book](https://rust-book.cs.brown.edu/), from two researchers at Brown University. Maybe you want to try that out instead of the standard version?

Next I'd recommend the 2nd edition of the book [*Programming Rust*](https://www.oreilly.com/library/view/programming-rust-2nd/9781492052586/). It's absolutely great and I've found that it is also a good reference to turn to first, rather than trying to search online.

Speaking of online help, you cannot go wrong with the [Rust users forum](https://users.rust-lang.org/). Search it for issues you have. Regularly browse it to see other questions people have and the solutions that are proposed.

There's also a plethora of Discord servers out there - two general Rust ones and it seems like one for every published crate. I personally dislike Discord because it walls off knowledge and is an absolutely horrible platform for trying to discover information (it was, after all, meant for gaming chat), but that's often where you have to go to get help for very specific things. If you can't find help elsewhere, see if the tool/framework/whatever has a Discord server and then start punching words into the search box and try to maintain patience as you wade through 50 different conversations to find what you're looking for.

Finally, something adjacent to this brief discussion is [Rust Edu](https://rust-edu.org/), which "is an organization dedicated to supporting Rust education in the academic environment."

That should get you started!

### Addendum: Other Resources

  * [The Mediocre Programmer's Guide to Rust](https://www.hezmatt.org/~mpalmer/blog/2024/05/01/the-mediocre-programmers-guide-to-rust.html)
  * [Compiler-driven Development: Making Rust Work for You](https://www.youtube.com/watch?v=_oaGNy3_798) - talk given by Jacob Pratt at RustConf 2024 that is beginner-friendly

### Addendum: Why learn Rust?

If you are here, you are probably already convinced that you want to learn Rust. But if not, I'm starting to collect some articles that praise Rust and explain why. I'm not sure if I'll keep this section or not - it may go away.
  * [Why Rust? It's the safe choice](https://maticrobots.com/blog/why-rust-its-the-safe-choice/)
  * [I built a startup in Rust, I would do it again.](https://cloak.software/blog/i-built-startup-in-rust/)

