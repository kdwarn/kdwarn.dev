+++
title = "Wood Chips Scored and Spread"
date = "2024-11-24"
tags = ["outdoors", "gardening"]
+++

A few days ago, an arborist was trimming a couple of the neighbor's trees and I decided to see if I could get some free wood chips. The idea came from listening to some episode (not sure which one) of [The Urban Farm Podcast](https://www.urbanfarm.org/blog/podcast/), where the host talked about a service called [ChipDrop](https://getchipdrop.com/). Arborists, apparently, have to pay to dump the wood chips they create during the course of their work. That service connects people who want wood chips with people who have them. But, as I learned, you can also just walk over and ask if they want to dump their chips in your driveway and that works too. So, I was the grateful recipient of 3 or 4 yards of free wood chips.

<figure>
  <img src="/images/outdoors/2024-11-20-wood-chips.jpg">
  <figcaption>Beautiful.</figcaption>
</figure>

I spread a few wheelbarrow's worth yesterday, and today I spread about half of them. Most went in a 4-to-5-inch layer where the next raised bed will be. I'll let them sit and start to decompose through early spring, and then will build the bed around them and top off with compost and dirt. I also mulched the two newest trees - white fringe tree and pagoda dogwood, both from the township's Shade Commission's free fall giveaway - and started on the blueberries. The remainder will be enough to finish the blueberries and mulch the two apple trees nearby.

<figure>
  <img src="/images/outdoors/2024-11-24-future-raised-bed.jpg">
  <figcaption>The new raised bed will be across from the existing one, and between the two there'll be a strip of wood chips to walk on. I'll modify the fencing to box in both the beds together. The blueberries are what's fenced in in the background.</figcaption>
</figure
  
