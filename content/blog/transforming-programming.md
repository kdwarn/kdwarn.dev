+++
title = "Transforming programming"
date = "2022-01-17"
tags=["daybook", "rust"]
+++

"Transforming programming" is the title of topic 30 of [The Pragmatic Programmer](https://en.wikipedia.org/wiki/The_Pragmatic_Programmer) (though a better title would probably be "transformational programming".) The gist is "we need to get back to thinking of programs as being something that transforms inputs from outputs". Uses example of Unix philosophy and piping. This makes a lot of sense to me, and in Rust it is essentially how iterators work - you keep chaining methods onto the iterator to do some work on each element in the collection, and then pass the outputs to the next method. This section also mentioned an `and_then` function, which Rust has, though they weren't talking about it in a Rust context.
