+++
title = "Instrumentation"
date = "2023-11-08"
tags=["daybook", "instrumentation"]
+++

I've heard the term "instrumentation" used - I think first and probably most on the [Oxide and Friends podcast](https://oxide.computer/podcasts/oxide-and-friends) (which makes sense now that I've learned a little bit about what it is) - but have never understood what is meant by it. Well, I spent five minutes and did a web search to find out, prompted by a blog post in my RSS reader using it. It's basically a system for discovering what's going on in software and getting a better view of state/performance/errors. So I think DTrace could be considered in this realm - and hence why I've heard about it on Oxide and Friends. Originally I thought it was more about deployment or infrastructure creation.
