+++
title = "Pagoda dogwood planted"
date = "2024-11-02"
tags = ["outdoors", "trees"]
+++

I planted a pagoda dogwood, courtesy of the shade tree commission. It was fairly easy digging compared to others. It went in the ground several feet to the southeast of the picnic table, for future shade.

<figure>
<img src="/images/outdoors/2024-11-02-pagoda-dogwood.jpg"/>
  <figcaption>About 5-foot-tall pagoda dogwood in its container on the spot where it was then planted.</figcaption>
</figure>
