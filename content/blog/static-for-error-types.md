+++
title = "'static for error types"
date = "2024-01-06"
tags=["daybook", "rust", "error-handling"]
+++

A suggestion from Jon Gjengset in the chapter on error handling in *Rust for Rustaceans*: 

> Finally, where possible, your error type should be `'static`. The most
immediate benefit of this is that it allows the caller to more easily propagate
your error up the call stack without running into lifetime issues. It also
enables your error type to be used more easily with type-erased error types,
as we’ll see shortly.
