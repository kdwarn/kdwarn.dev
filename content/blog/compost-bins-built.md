+++
title = "Compost bins built"
date = "2023-06-10"
tags = ["outdoors", "gardening"]
+++

After a bit of searching the web, I found a compost bin design I liked, written up in "[How to Build a DIY Compost Bin For Rich Amendments](https://www.epicgardening.com/diy-compost-bin/)" by Sarah Jay. Aside from only doing two bays, I also simplified the design a bit and left off the inner 2x4s on the end pieces, in both front and back. 

I'm writing this a year later, and there's been no issues with it, so I'd say it was a success. During the first six months, I was surprised at how quickly the food and yard waste would lose mass. Partly I think this was caused by various animals (birds, fox, and groundhog that I saw in it, and I assume deer, chipmunks, and squirrels too) eating from it, but mostly I assume from water loss. So I started putting in more yard waste, including grass clippings a couple times. One bay is now about half full.

Here's a picture of them just a couple days after being built and placed into a cleared spot, without any front boards on yet:

<img src="/images/outdoors/2023-06-13-compost-bins.jpg">
