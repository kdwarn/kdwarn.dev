+++
title = "Pumpkin and the fox"
date = "2024-06-07"
tags = ["outdoors", "gardening", "vegetables", "wildlife", "trees"]
+++

This pumpkin is currently my favorite plant. Look at it! It was about six inches tall two weeks ago. I'm training it up the wooden stake so it will have more room; we'll see how it survives once I get it up over the side. Deer snack? Maybe. Though I have another growing outside the raised bed and it has been untouched so far, knock on wood.

<img src="/images/outdoors/2024-06-07-pumpkin.jpg">

Speaking of wildlife, a bit earlier I had finished putting protection around [the two newly planted hickories](/blog/two-hickories-planted) (because they've already been nibbled by deer), and then ran into the fox. Not sure if it's always the same fox I see, but likely? We had been walking towards each other from two different paths on the terrace. I was unaware of it until I turned onto the path it was on, which was coming from the raised bed and compost area. I'm sure the fox had already noticed me, though I think both of us were surprised. I backed up and it slowly but fairly nonchalantly continued on its way, marked its territory on a juniper I had planted, and then ended up where I took these pics a couple minutes later. First curious, then indifferent.

<img src="/images/outdoors/2024-06-07-curious-fox.jpg">
<br/>
<img src="/images/outdoors/2024-06-07-indifferent-fox.jpg">
