+++
title = "let if"
date = "2022-10-13"
tags=["daybook", "rust"]
+++

I've used the `let if` pattern before, but not enough to remember how it works, so had to refresh myself on it. It's very useful. Here's a sample from [tp-updater](https://github.com/dvrpc/tp-updater):

```rust
let updated_indicators = if self.updated_indicators.is_empty() {
    html! { <p>{ "There are no recently updated indicators."}</p> }
} else {
    html! { <p>{ format!("{:#?}", self.updated_indicators) }</p> }
};
```

