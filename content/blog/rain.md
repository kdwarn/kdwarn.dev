+++
title = "Rain!"
date = "2024-11-21"
tags = ["outdoors", "gardening"]
+++

It's been raining the last 3 days! That ! is excitement, because there has been almost no rain here going on at least a month and a half. There was a brief, light rain about ten days ago, but otherwise it's been so dry. Now everything has had a great soak and I'm less worried about the things I've recently planted, but also grateful that I now have at least a week off watering things, if not much longer.
