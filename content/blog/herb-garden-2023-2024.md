+++
title = "Herb garden in 2023 and 2024"
date = "2024-12-14"
tags = ["outdoors", "gardening", "herbs"]
+++

I was planning on writing this post about 2024 only, but it turns out I didn't write anything about 2023 yet, so I'm going to cover both years. It took a bit of digging through old photos and unfortunately I only found a couple for 2023. Many more for this year though. Next year I'll try to be a bit more organized to make a year-end review a bit easier.


## 2023

In spring 2023, I desodded a small bit of the lawn next to our back patio for the herb garden. I planted the back quarter of it with basil, marjoram (a subspecies of oregano), thyme, and rosemary. All but the basil I bought from a local garden center. The basil was just from buying fresh at the store and then rooting. I kept adding more basil for a couple months, so it was a bit fuller by mid-summer.

<figure>
  <img src="/images/outdoors/2023-05-06-herb-garden.jpg" alt="Small herb garden, mostly soil, with plants growing in the back quarter of it." />
  <figcaption>Herb garden, May 6, 2023</figcaption>
</figure>

In July, I had harvested and dried some thyme, basil, and rosemary. I also did so again in October, and this time was also able to get some marjoram:

<figure>
  <img src="/images/outdoors/2023-10-20-herbs-dried.jpg" alt="Four labelled spice jars. The marjoram and basil are full; thyme is about 1/3 full, and the rosemary is in large jar and half full." />
  <figcaption>Dried herbs in jars, October 10, 2023</figcaption>
</figure>

## 2024

This year, I planted the whole space. In all, it's somewhere around 15 square feet. 

The rosemary, marjoram, and thyme that I'd planted last year all survived the winter and spread a little. The rosemary probably grew the most - taller and bushier with a little bit of pruning. The marjoram probably tripled the ground it covered and then in mid-June sent a lot of stalks up into the air, most of which I allowed to flower and go to seed. I wish thyme was faster growing than it is - next to basil that's probably the herb I used the most, at least among those I grow. So I split it and planted half of it at the front corner. I'll probably split it again next year so there's more of it. I harvested and dried all three of those. I now have [more rosemary than I know what to do with](/blog/rosemary-2024-oct), a bit of marjoram, and, sadly, no more thyme.

<figure>
  <img src="/images/outdoors/2024-05-01-herb-garden.jpg" />
  <figcaption>Original planted section of herb garden, May 1, 2024</figcaption>
</figure>
<figure>
  <img src="/images/outdoors/2024-05-06-herb-garden.jpg"/>
  <figcaption>Herb garden, May 6, 2024</figcaption>
</figure>

Parsley, cilantro, and lavender were new additions this year. I also attempted to plant some sunflowers on the edges of it, but, though they ignored them for a while, the deer eventually ate them. I also threw in a cucumber among the basil, because I had grown it from a seed but didn't have space for it in the vegetable garden at the time. After a couple things didn't survive in the vegetable garden, I moved it there.

<figure>
  <img src="/images/outdoors/2024-06-11-herb-garden-annotated.jpg"/>
  <figcaption>Annotated herb garden, June 6, 2024</figcaption>
</figure>
<figure>
  <img src="/images/outdoors/2024-06-18-herb-garden.jpg"/>
  <figcaption>Herb garden, June 18, 2024</figcaption>
</figure>
<figure>
  <img src="/images/outdoors/2024-06-29-herb-garden.jpg"/>
  <figcaption>Herb garden, June 29, 2024</figcaption>
</figure>

The basil, Italian Genovese grown from seed, was the great success of the year. I aggressively pruned the plants and by the end they were nice and bushy and about 20 inches tall. Anytime I needed fresh basil it was available. I gave some to my neighbor. I also [dried a bunch](/blog/basil-2024-oct) and made two batches of pesto. The second batch was big, and in addition to the couple of meals it provided a sauce for at the time, I think there's probably enough in the freezer for three or four more. I'm looking forward to seeing how well it has fared. About a month ago it got hit by frost. Half of the 15 or so plants turned completely brown, while the rest lost most of their leaves but stayed green for another week or so. Just before spring I'll cut them off at their base and chop them up into the compost.

<figure>
  <img src="/images/outdoors/2024-07-27-basil-harvest.jpg"/>
  <figcaption>One of many basil harvests, July 27, 2024</figcaption>
</figure>
<figure>
  <img src="/images/outdoors/2024-07-27-bee-on-marjoram.jpg"/>
  <figcaption>Bee on marjoram flower. July 27, 2024</figcaption>
</figure>
<figure>
  <img src="/images/outdoors/2024-08-02-thyme-harvest.jpg"/>
  <figcaption>Harvested and semi-cleaned thyme, August 2, 2024.</figcaption>
</figure>

The cilantro was the first thing to go, long before frost. Although I got a few handfuls from it, it didn't do that well. But I let a few go to seed, which I'll use for next year.

Surprisingly, the parsley is still alive and doing well. It did not start off well - something was eating it when it first started coming up, but then let it go. With how delicate the leaves are, I figured it would have been the first thing to succumb to frost, but despite several periods of below freezing temperatures for multiple days, it's still there. Which reminds me - I should grab some before it's gone. Like the cilantro, I got a few handfuls from it over the summer and fall.

<figure>
  <img src="/images/outdoors/2024-10-15-herb-garden.jpg"/>
  <figcaption>Herb garden, October 15, 2024. You'll notice there's a single basil plant among the cilantro. Since the cilantro wasn't doing very well and the basil was pretty crowded, I moved it there.</figcaption>
</figure>
<figure>
  <img src="/images/outdoors/2024-10-15-herb-garden-from-side.jpg"/>
  <figcaption>Herb garden from the side, October 15, 2024</figcaption>
</figure>
<figure>
  <img src="/images/outdoors/2024-12-09-herb-garden.jpg"/>
  <figcaption>Herb garden, December 9, 2024</figcaption>
</figure>
