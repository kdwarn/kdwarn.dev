+++
title = "Planted remaining sunflowers"
date = "2024-05-29"
tags = ["outdoors", "gardening"]
+++

In the morning, I planted the last of the sunflowers that I had under lights in the basement. There were 4 in one pot and 2 in another. The roots were bound well together so I just planted them by the pot - four went into the decaying center of the large tree stump on the terrace, and two by the sidewalk. Overnight, the ones by the sidewalk had already been chomped - but I'm not sure by what. They were just cut off at a stalk segment, but not eaten. The tops just lay on the ground next to the stalk stumps that were about 4 inches tall. The ones in the tree stump survived. 

In all, that's about 50 sunflowers I planted. About 20 have survived, with most doing well except the ones on the hillside that haven't grown much and are a bit spindly. I think next year I'll try a few less and also wait to put them out maybe two weeks later - so maybe around May 1.

One tray of the seedlings inside on April 3:

<img src="/images/outdoors/2024-04-03-sunflower-seedlings.jpg">
