+++
title = "Raised bed started"
date = "2023-05-08"
tags = ["outdoors", "gardening", "vegetables"]
+++

Here's the first picture I have of the raised bed I made. Compared to the two other small building projects, this one was really a breeze: three 2x6s, 10 feet long, cut one of them in half, screw them together. Most of the work was hauling up dirt to fill it in. Thankfully, I suppose, there was a ton of excess soil/mulch around two sides of the house that needed to be removed and so it went into the bed. 

<img src="/images/outdoors/2023-05-08-raised-bed.jpg"/>

Here is the lumber I had picked up for this and the other projects the week or so before:

<img class="center" src="/images/outdoors/2023-04-30-lumber-in-shed.jpg"/>
