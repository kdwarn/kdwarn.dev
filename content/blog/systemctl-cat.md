+++
title = "systemctl cat"
date = "2023-08-18"
tags=["daybook", "systemd"]
+++

`systemctl cat <service-name>` will print the path and contents of the unit file for a service. I sometimes know the filename and so it's easy enough to just `cat` the path, but sometimes I don't and I guess several times until I give up and look it up. Just using this by default would probably end up saving some time/make things smoother.
