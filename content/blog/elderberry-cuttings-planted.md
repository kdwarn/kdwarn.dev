+++
title = "Elderberry cuttings planted"
date = "2025-02-02"
tags = ["outdoors", "gardening", "fruit"]
image = "/images/outdoors/2025-02-02-elderberry-cutting.jpg"
+++

In September, I ordered a dozen hardwood American elderberry cuttings from [River Hills Harvest](https://www.riverhillsharvest.com/), based out of Missouri. I was inspired to do so after listening to an episode ([817](https://www.urbanfarm.org/2024/06/07/817-john-moody/)) of The Urban Farm Podcast, interviewing [John Moody](https://johnwmoody.com/) about [a book he wrote about elderberries](https://johnwmoody.com/product/the-elderberry-book/).  The ones I bought are the Adams 2 variety of *sambucus canadensis*, which RHH describes as "Early ripening with very large clusters of purplish/black berries. Sweeter than most elderberries and somewhat self-fruitful. One of the oldest cultivars from New York." They go on to say they are indeterminate, can grow between 8 and 10 feet tall, and have excellent yields. (Hurray for the birds and whatever other wildlife will get them!)

At the end of January I got a notice that they had shipped and a couple days later they were on my doorstep. They were about eight inches long, between 3/8" and 1/2" in diameter, and had two pairs of buds four or five inches apart. The top cut is straight across, and the bottom is angled. They recommend putting the bottom pair of nodes two to three inches beneath the soil. Full planting instructions can be found on [this page](https://www.riverhillsharvest.com/grow-elderberry-classes) of their site.

Over the course of four hours, I desodded grass, dug up rocks in the way, planted the cuttings, mulched them, and then finally surrounded each one with chicken wire to protect them from deer. Looking forward to see how they do!

<img src="/images/outdoors/2025-02-02-elderberry-3-pack.jpg">
<br/>
<img src="/images/outdoors/2025-02-02-elderberry-cutting.jpg">
<br/>
<img src="/images/outdoors/2025-02-02-elderberry-planted-and-protected.jpg">
