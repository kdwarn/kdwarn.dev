+++
title = "Planted juniper cuttings"
date = "2024-03-10"
tags = ["outdoors", "gardening"]
+++

I had taken about five cuttings from the owl gray juniper that we planted on the hillside with the apple trees, and then kept them inside for months under lights, regularly watering them. So on this day, I planted them on the hillside.

Spoiler from a few months in the future: none of them survived. At least two were probably because I didn't water enough. Another was because of being hit with a weedwhacker. Not sure why the other two didn't make it.
