+++
title = "just"
date = "2024-02-14"
tags=["daybook", "runner"]
+++

I've never used `Make` before (as someone creating the `Makefile`), but I just tried out [`just`](https://just.systems/) and found it to be fairly intuitive/well documented. There were a couple things I struggled with, but nothing too serious: composing recipes has some nuance that I didn't pick up on at first, silencing output of commands/recipes I got to work how I wanted but mostly by luck. The end result is that the next time the work needs to be done, using `just` will be a far better experience than the step-by-step directions it is replacing. 

I feel like Just is a good sibling tool to Ansible. Ansible would have been overkill for what I did with Just, as well as harder to set up for a new person. `just` is a simple binary, no minimal boilerplate to speak of, simple syntax. I'm looking forward to seeing where else I can use it. I expect that the next `justfile` I create will come along much faster than this first one did.
