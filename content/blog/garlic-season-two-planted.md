+++
title = "Garlic season two planted"
date = "2024-10-05"
tags = ["outdoors", "gardening", "vegetables", "garlic"]
+++

Last year, [I planted about 40 garlic cloves](/blog/garlic-planted). Most survied, except three or four that seemed to be rotting at the neck and that I yanked out at some point. Not sure why that happened. The rest did well, though they could have been larger. After harvesting in July, I set aside the largest ones for replanting in [the new bed I made for them](/blog/made-raised-bed-for-garlic). So today, in this new 16-square-foot area, I planted 64 cloves (8 rows of 8). I took a little under an hour, and probably half that time I was just breaking apart the old heads. It went fast; I was catching up with my friend Paul, who was driving to a protest in Cleveland, while I planted.

This year, unlike last year, I covered the top with dried grass clippings after planting. I think I also pushed them into the soil a little deeper - I was shooting for about 3 inches down. I'm looking forward to seeing how they turn out, and if there's any difference in the size compared to last year. Each has slightly more room than those from the previous set. I'm hoping 64 plants will be enough to get us through most of the year, with 8 dedicated for the planting next year. But I actually have no idea how much garlic we go through in a year. 

<img src="/images/outdoors/2024-10-05-garlic-planted.jpg">
