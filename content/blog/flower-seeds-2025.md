+++
title = "Flower seeds started"
date = "2025-03-05"
tags = ["outdoors", "gardening"]
image = "/images/outdoors/2025-03-07-flower-seeds-started.jpg"
+++

Over the past week, I started a bunch of flower seeds. Roughly:
  - 45 common milkweed (from my yard)
  - 45 butterfly milkweed (from my yard)
  - 7 swamp milkweed (from local nature center seed library)
  - 66 bee balm (from my mom)
  - 130 columbine (from my mom)
  - 40 obedient plant (from my mom)
  - 20 daisy (from my yard)
  - 80 blue-eyed grass (from local nature center seed library)
  - 13 swamp rose mallow (from local nature center seed library)
  - 8 unknown (from a seed pod I'd snagged while traveling in central PA; curious to identify it)

That's 450 seeds in all! For most of them, I only used half of what I have, and I'm going to directly sow the remaining ones outside in the next few weeks. (Except for the common and butterfuly milkweeds - I probably have 1000 of those.)

All except the bee balm were cold stratified in the fridge for two months, which I think is cutting it close. The columbine I did only dry, but all the rest were half dry and half moist. For the moist stratification, I used unbleached coffee filters (which held up very well). I'd first moisten a filter on a plate with a thin layer of water on it, hold it up to let the excess water drip off, and then put it on another plate before adding seeds and wrapping it up to about the size of a half a roll of pennies. Then the filter went into a ziplock bag, labelled, and finally when they were all done, they went into a small plastic container. Similar thing for the dry stratification, just minus the coffee filters. Both containers then went into the back of the fridge, where I patiently waited for them to do their thing. And now I'm waiting for them to do their next thing.

Soon I'll need to figure out where I'm going to start the vegetable seeds, because there's not a hell of a lot of space left under grow lights. I'm hoping I can move the flowers to windowsills once they start to come up to make way for the veggies.

<figure>
  <img src="/images/outdoors/2025-03-07-flower-seeds-started.jpg">
  <figcaption>About 35 pots under two growlights on/in a refurbished record player console that now serves as my planting station.</figcaption>
</figure>
