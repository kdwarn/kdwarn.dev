+++
title = "Planted spinach and beans"
date = "2024-05-29"
tags = ["outdoors", "gardening", "vegetables"]
+++

I planted spinach and more french filet beans in the places where other things hadn't come up or survived, which was basically all of the soybeans, most of the carrots, half of the beets, and a few french filets that had been chomped. If the beans do well, they'll now be probably half of the garden.
