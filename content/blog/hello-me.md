+++
title = "Hello me!"
date = "2024-05-31"
tags = ["outdoors"]
+++

Although I am putting this on my website, this section is pretty much entirely for myself. If someone else other than me stumbles across this and enjoys it, great! Surprising, but great! Mostly I want to curate some of my journaling and logging into something more refined so that I can look back on how the outdoors area of my house has changed, so actually this is more for future me. Hello future me!
