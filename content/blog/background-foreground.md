+++
title = "Background and Foreground"
date = "2021-08-29"
tags=["daybook", "terminal"]
+++

This is more something I relearned than learned for the first time.

Use Ctrl-Z to put a program you are running from a terminal into the background. You will be taken back to the terminal. Can do other stuff. Then use `fg` to bring the program you put into the background back into the foreground - back to the active program in the terminal. I think this will be useful in Vim when I don't need to have a dedicated terminal running in a window.

