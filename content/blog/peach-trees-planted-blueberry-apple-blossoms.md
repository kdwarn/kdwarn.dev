+++
title = "Peach trees planted, blueberry and apple blossoms"
date = "2024-04-26"
tags = ["outdoors", "fruit", "trees"]
+++

I took the day off work so I could plant two peach trees and do other yard work. The trees had arrived the weekend before - if not before that - from [Cummins Nursery](https://www.cumminsnursery.com/), based out of Ithaca, NY, the same nursery where I had got the four apple trees the year before. The peach trees were supposed to both be grade 1, but they had miscounted their inventory, so I had one grade 1 and one grade 4. Both are Challengers, a yellow flesh peach. The description from [their page](https://www.cumminsnursery.com/buy-trees/product-detail.php?type=tree&id=20968) for the cultivar:

> A very cold-hardy, freestone peach with excellent disease resistance. Also known as NC-C3-68.
>
> Challenger is very cold hardy. Its flowers buds, bloom, and young fruit all demonstrate high tolerance for freezing temperatures. This tree also has excellent resistance to bacterial spot. It is self-fertile and does not need a second variety present as a pollenizer.
> 
> Challenger is an improved descendent of Reliance. It has a larger fruit with more attractive coloring, firmer flesh, and improved texture. The peaches are medium sized with a red, low fuzz skin.> 
> Crossed in 1987 and selected in 1990, this cultivar is a cross of Redhaven with a breeding selection derived from Reliance and Biscoe. It was developed by NCSU.

One I put on the terrace, about 15 fifteen in front of the two apple trees up there, and the other in the backyard near the property line with AM. Peach trees are surprisingly small. These are both standard trees, and will grow to [12 to 13 feet tall](https://www.cumminsnursery.com/learn-trees/fruit-tree-height-and-spacing). I'm not sure if this is true of all peach trees, but these ones are self-fertilizing.

I also noticed that the largest of the four blueberry bushes I planted last spring had a few blossoms on it. And so did two of the four apple trees - the Arkansas Black (below) and also the Winecrisp.

<img src="/images/outdoors/2024-04-26-black-arkansas-blossoms.jpg">
