+++
title = "Capturing Catchall Test Value"
date = "2024-01-18"
tags=["daybook", "rust"]
+++

I seem to have forgotten that when pattern matching in Rust, you can capture the value of a catchall test (left side of arm). I typically just use `_`, which will match anything, but isn't captured for further use. By creating a variable instead, it can be used on the right side of the arm, for whatever purpose. Realized this from watching [Zoo's Rust Club: Error-handling](https://www.youtube.com/watch?v=IdnjG5N2l4M).
