+++
title = "Iterators and collect()"
date = "2022-01-08"
tags=["daybook", "rust"]
+++

Figured out [rustlings/iterators2](https://github.com/rust-lang/rustlings/blob/main/exercises/18_iterators/iterators2.rs). Now the solutions seem obvious. I think what I was doing wrong was thinking that `.collect()` created a vector, whereas it creates whatever kind of collection you tell it to or that it infers. In this exercise, there were two functions requiring you to have a different result type, but the code was the same for both. I was making it much harder than it was, not taking advantage of `.collect()` and Rust's ability to infer the type.

Before solving the exercise correctly, I spent about two hours reading through [the documentation on iterators](https://doc.rust-lang.org/stable/std/iter/index.html) as well as part of "[A Journey into Iterators](<https://hoverbear.org/blog/a-journey-into-iterators/>)" by Ana Hobden. Both were very helpful. I discovered the `.inspect()` method in the Hobden piece, and it seems like it will really be useful in debugging/figuring out iterator chains.

