+++
title = "Black walnut planted"
date = "2024-05-18"
tags = ["outdoors", "trees"]
+++

No picture yet, but I transplanted the black walnut from MH/DW. Surprisingly easy removal; I think it took all of 5 minutes to get it out. Easy planting too. It's in front of the hickories I planted.

It was much too late in the year for a transplant and it's been struggling since, but I hope that it'll survive.
