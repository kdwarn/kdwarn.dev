+++
title = "More basil dried and stored"
date = "2024-10-06"
tags = ["outdoors", "herbs", "gardening"]
+++

I harvested a bunch of basil yesteday and dried it in the dehydrator borrowed from MH for something like 4 hours. Early this morning - since the kiddo woke up briefly and I wasn't able to fall back asleep - I crushed it up and funneled it into a jar. I didn't measure it, but I'd guess it was 4 cups fresh, which reduced to about 3/4 of a cup dried. I also did this last month, with a slightly smaller amount (because I ruined half of it by trying to air fry it in the oven), and also have some from earlier in the year and from last year, so we're all set on dried basil for some time. Still, if the plants go to seed (not sure they will flower this late in the year), I'm going to save them for next year, for either fresh basil, pesto, or drying and giving away. The variety is Italian Genovese.

<img src="/images/outdoors/2024-10-06-dried-basil-leaves.jpg">
<br>
<img src="/images/outdoors/2024-10-06-basil-in-jar.jpg">
