+++
title = "Transplanted Second Oak"
date = "2024-10-11"
tags = ["outdoors", "trees"]
+++

Over the course of five hours, I dug up, drove, and planted a second oak tree, from MH/DW's place. This one was about 8' tall, just a bit taller than than the one MH and I had transplanted from their place a year(?) prior. I had originally planned on bonsaiing it, and so about two years ago I topped it at about 12". At that time, it had probably been 4' tall. Grew back with a vengeance. Two-thirds of the five hours was digging it out. It was tiring, dirty work, and I wasn't able to get the tap root out complete. The drive in the little car was thankfully smooth, and so was digging the big hole to put it in. I also found a cool surprise while digging the hole at home: two glass bottles made in my hometown. If my dad were alive, he'd probably be able to tell you what they were and the approximate year they were made.

<img src="/images/outdoors/2024-10-11-oak-roots.jpg"/>
<br/>
<img src="/images/outdoors/2024-10-11-hole-for-oak.jpg"/>
<br/>
<img src="/images/outdoors/2024-10-11-bottle.jpg">
<br/>
<img src="/images/outdoors/2024-10-11-oak-in-new-home.jpg">
<br/>
