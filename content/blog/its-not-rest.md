+++
title = "It's Not REST"
date = "2024-08-28"
tags=["daybook", "http", "rpc"]
+++

I've been building "REST" APIs for years that are not actually REST. (See ["How Did REST Come To Mean The Opposite of REST?"](https://htmx.org/essays/how-did-rest-come-to-mean-the-opposite-of-rest/) as a starting point). I've kind of known this for a while and I've always meant to get back to and try to fully understand HATEOAS and other similar concepts. But for whatever reason - mainly, the things I built did what they were supposed to do (deliver data) - I never have. And I'm completely fine with that. REST seems overly complex and rigid. I don't need the reponse to be "self-describing" or contain the possible actions from there, and I don't really want to read yet another thing about the nuances of [hypertext](https://roy.gbiv.com/untangled/2008/rest-apis-must-be-hypertext-driven) or hypermedia. I just need the thing to provide data. I think external, user-facing and higher-level documentation is useful.

So, what is it instead? I think HTTP-based [RPC](https://en.wikipedia.org/wiki/Remote_procedure_call). And that's all that's really needed.

