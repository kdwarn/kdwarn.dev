+++
title = "Started holly hedge (again)"
date = "2024-10-19"
tags = ["outdoors", "gardening"]
+++

In March, I spent a couple hours planting a holly hedge out front along the road, from very short cuttings I had taken months prior and put under grow lights in the basement. It did not work out. The first hint that that was going to be the case was when I was planting them and only three or four (out of around 30) had any roots. Beyond that, I think I was not helped by an unusually warm early spring. Nor with how short the cuttings I made or thinking that putting them inside would help.

So, I've tried again, but this time armed with a bit more research. Mainly, it was this "Mark's House and Garden UK" [video](https://www.youtube.com/watch?v=kejTtmzPDr0). Pretty much followed how he did things, though my cuttings were often shorter given how much existing holly I had to work with. They ranged from 12" to maybe 30" for the few tallest. In all, I planted 36 of them, spaced 24" apart, in a line 3 feet from the road. The first 33 ended where I'd previously planted a few false hollies and some as-yet-unknown tree, also 3 feet from the road and spaced about the same width apart, and then the other 3 after that. Watered them, cleaned up.

That was yesterday. Today, I dug some compost out of the bins and put a shovelful around their bases, then watered them again. A delivery of mulch is coming Monday and so I'll soon spread mulch around them. 

Here's hoping this attempt fares better than the last.

<img src="/images/outdoors/2024-10-19-holly-hedge.jpg">
