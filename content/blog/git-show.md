+++
title = "git show"
date = "2024-04-01"
tags=["daybook", "vcs"]
+++

Occasionally I'm looking at the current code for a project, and want to see some previous iteration of it, often because I want to bring back some part of it. I'll typically go to the code in the software forge, find the commit I think I'm looking for, click the link to view the source at the point in the commit history, and then navigate to the file. 

There's an easier way: 
  1. `git log` to find the commit
  2. copy first bunch of characters (6) of the hash for the commit you want to view code from
  3. `git show [hash]:relative/path/to/file`.

That goes to stdout. Pipe it to an editor, e.g.: `git show 3a53e76:src/main.rs | hx`. I haven't yet figured out how to set the language of the file when opening with Helix in this way, but that can be solved by setting it once the file is open with `:set-language rust`.
