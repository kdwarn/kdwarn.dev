+++
title = "First Chestnut Planted"
date = "2022-12-27"
tags = ["outdoors", "trees"]
+++

I planted the chestnut given to me by MH, which came from the [American Chestnut Foundation](https://tacf.org/), ID W9-20-115. It's about six feet in front of the maple on the western line.
