+++
title = "at"
date = "2024-12-17"
tags = ["daybook"]
+++

Want to run some program, but perhaps not right at the moment? `at` to the rescue. 
 
See:
  * `man at`
  * <https://tecadmin.net/one-time-task-scheduling-using-at-commad-in-linux/>
