+++
title = "Rebase and Merge"
date = "2021-07-05"
tags=["daybook", "vcs", "python"]
+++

Rebasing in `git` is similar to merging. You do it when you want to combine the changes from one branch into another. (Although you can also do it interactively within one branch to squash commits, which I've done a fair amount since figuring out how that works.) I don't understand the differences between the two strategies, but at least `rebase` is a little less mysterious. I learned this from [Git Gud](https://github.com/benthayer/git-gud/), a Python program that interactively teaches you `git` by having you try to do various tasks with it. The explanations of how things work are great (which you can view with `git gud explain`). (Side note: I don't know how this CLI is able to use `git gud` as its command - shouldn't `git` be immediately invoked and tell you it doesn't know wtf the `gud` subcommand is?) Anyway, also learned from that program what `~` and `^` do (at the end of a branch name or hash). I've used `HEAD~` and `HEAD~2` or similar before (again, with interactive rebasing), but now I understand what's going on. (`~` is for the previous commit, `~N` is for the nth previous commit; `^` is similar but for parent branches.)
