+++
title = "Puttering"
date = "2025-01-29"
tags = ["outdoors", "wildlife", "gardening"]
+++

"Puttering" to me means walking around your garden and/or yard, often with some kind of beverage in hand - maybe coffee in the morning, maybe beer in the afternoon - and doing the occasional work you hadn't necessarily planned to do but figured why the hell not. This at least is the definition that was handed down to me by my parents. I recently learned that it's "pottering" in the UK, and I'm kind of surprised it took me this long to hear the UK-English version of the word.

So I puttered for a bit around the yard today, between the end of work and nearly sundown. It was very relaxing and I think needed. First it was dumping the compost and filling the birdfeeder, dropping an ear of corn for deer. Then I noticed just how much deer poop (and some rabbit, I think) was around and so I grabbed a shovel and slid the little pellets onto it, dumping them into a bucket I was carrying around, and then dumped that on the spot where I'll be building a second raised bed for vegetables this spring. Grabbed a package that was delivered off the front porch. Then decided to get a start on making small cages for the elderberry cuttings that I'll be planting soon. I think I already have nine that will work, at least for the first year, so just three more to go. I had a portion of chickenwire left over from other caging needs, and so cut out three pieces, got two of them twisted up into little columns.

Then it was getting a bit dark, so I left the last one and the tops to do another time. The motion-detector light was on in the side yard, but I didn't see any deer out as I went inside. They either spotted me and left, or it was just set off by the holly being blown around by the gusts of wind. Or a fox did a quick round looking for something to eat.
