+++
title = "American hornbeam planted"
date = "2024-05-11"
tags = ["outdoors", "trees"]
+++

The son of neighbor MM/SM picked this up from the township's Shade Commission giveaway on April 27 since I wasn't able to go, and it sat on the back patio until today. It was bigger than I expected - nearly 7 feet tall. I put it on the southwest edge of the yard, so it's now the first in the line of trees along the western boundary. 
