+++
title = "Third raised bed built"
date = "2025-02-09"
tags = ["outdoors", "gardening", "vegetables"]
image = "/images/outdoors/2025-02-09-new-raised-bed.jpg"
+++

Nearly two years after I [built the first large raised bed](/blog/raised-bed-started) and five months since I [built the small one for garlic](/blog/made-raised-bed-for-garlic), I've added a third raised bed, just a bit smaller than the first one (4 feet x 10 feet, rather than 5 feet x 10 feet). Five feet is just a bit too wide to be able to comfortably reach into the middle from the sides; I think four feet will be better.

It was just as easy to build as the first one: buy three boards, cut one in two, screw them together. This time, though, I built it on the floor of the shed (one end hanging out the open doors), rather than in-place, so it would be easier to square the boards up. So I then had to carry/slide the finished frame from the shed through the yard and up the hill to the terrace. I wouldn't have been able to do it myself had it been much bigger, but it ended up being easier than I thought, thankfully.

It's about half-filled with decomposing wood chips that [I got late in the fall](/blog/wood-chips-spread), a bucketful of deer poop, and some ashes from the fireplace. Sometime in the next few weeks I'll top if off with compost and soil, and then work on extending the fencing from the other raised bed to encompass both of them. Before I do that, though, I'll move the asters that are growing in the middle of it.

Now that I have three raised beds and two compost bins, I think I'm going to start referring to it as my farm, tiny as it is.

<figure>
  <img src="/images/outdoors/2025-02-09-new-raised-bed.jpg">
  <figcaption>A new raised bed. It's about half-filled with decomposing woodchips.</figcaption>
</figure>

<figure>
  <img src="/images/outdoors/2025-02-09-the-farm.jpg">
  <figcaption>The farm! One 4x10 raised bed, one 5x10 raised bed, one 4x4 raised bed, and a two-bay compost area. Caged blueberry bushes are in the background.</figcaption>
</figure>

