+++
title = "Use format! to turn chars to string"
date = "2022-06-30"
tags=["daybook", "rust"]
+++

While working on a Rust exercise, at first I did some convoluted way of turning two chars into a string. But it's quite simple with the `format!` macro: `format!("{ch}{next_ch}")`.

