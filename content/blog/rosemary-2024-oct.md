+++
title = "More rosemary dried and stored"
date = "2024-10-06"
tags = ["outdoors", "herbs", "gardening"]
+++

No pics on this one, but I trimmed the top of about 25 stems of the rosemary plant, which soon will graduate to rosemary bush. I then trimmed off the leaves from the stems, and baked in the oven at 180 degrees for 20 minutes and then (because it didn't seem like 180 was doing the trick) at 210 for another 20. Got nearly a full spice jar. I still have a bunch leftover from last year. I'm going to need to find more uses for rosemary. 

I then took all the stems and put them in the ground in various places around the yard. Not expecting much - if one survives I'll be happy. Mostly I just like the way it looks.

