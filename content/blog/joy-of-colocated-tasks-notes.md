+++
title = "The Joy of Colocating Tasks and Notes in Plain Text"
date = "2024-12-21"
tags = ["productivity"]
+++

My [previous post](/blog/fall-2024), about gardening/yardwork for the fall season, was not long nor in-depth, but it was fairly comprehensive and quick to do. I could have spent more time on it, but I didn't need to. And not needing to is the important thing: this was the second of such posts I wrote, and I knew it was going to be easy because of the system/routine I'm working out. Mainly, that is writing about projects in a [zettelkasten-like system](/blog/a-zettelkasten-with-vim-and-bash), keeping tasks about those projects along with their notes, and using the [`taskfinder`](/projects/taskfinder) TUI I built on top of it to stay on top of tasks. Project notes and tasks live together, and the lack of context-switching is great. On top of that, it's all stored locally on my computer, which has the benefit of not requiring an internet connection and the obvious privacy benefits. Of course, this does not scale to collaborative efforts, at least not now. But it works well for my needs, and that is good.
