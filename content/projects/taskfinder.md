# Taskfinder

Keep your project-related tasks where they belong - with your notes on the project! This program, a terminal user interface (TUI), will extract and display them.

Installation, usage, etc. can be found in the project's readme at [codeberg](https://codeberg.org/kdwarn/taskfinder).

<br/>

> <span class="small quote">a really simple but really nice way of doing to-do items. [...] this one is pretty enough that I might keep going with it.</span>\
— [Félim Whiteley on episode 305 of Late Night Linux](https://latenightlinux.com/late-night-linux-episode-305/)

<br/>

## Screenshots

There are six possible modes that the app can be in - files, tasks, log, configuration, help, and, optionally, evergreen. Below are screenshots of a few of them. Click for full-screen versions.

### Files Mode

Files is the main mode - you can browse your files that have tasks in them and potentially edit them from the app.

<a href="/images/tf_demo/tf-files-mode.png"><img src="/images/tf_demo/tf-files-mode.png" width="900px" title="files mode"></a>

### Tasks Mode

Tasks mode allow you to view all incomplete tasks (sorted by due date/priority) or all completed tasks (sorted by completed date/priority).

<a href="/images/tf_demo/tf-tasks-mode.png"><img src="/images/tf_demo/tf-tasks-mode.png" width="900px" title="files mode"></a>

### Log Mode

The log starts off in table view, but you can also view it as a chart.

<a href="/images/tf_demo/tf-log-mode-table.png"><img src="/images/tf_demo/tf-log-mode-table.png" width="900px" title="log mode, table"></a>

<a href="/images/tf_demo/tf-log-mode-chart.png"><img src="/images/tf_demo/tf-log-mode-chart.png" width="900px" title="log mode, chart"></a>

### Config Mode

Edit the app's configuration in this mode (though you can also edit the configuration file directly).

<a href="/images/tf_demo/tf-config-mode.png"><img src="/images/tf_demo/tf-config-mode.png" width="900px" title="config mode"></a>

