# Projects

This is a brief overview; further details and documentation can be found by following the links. If you've found any of my personal open-source work valuable, you are welcome to <a href="https://liberapay.com/kdwarn/donate">send a donation</a>.

### veganbot <span class="small">(Rust)</span> 

Fediverse bot for posting vegan things.\
[repository](https://codeberg.org/kdwarn/veganbot)

### [taskfinder](/projects/taskfinder) <span class="small">(Rust)</span>

Keep your project-related tasks where they belong - with your notes on the project! This program, a terminal user interface (TUI), will extract and display them.

### monitor-sites <span class="small">(Rust)</span>

Monitor website availability, with optional desktop notifications.\
[repository](https://codeberg.org/kdwarn/monitor-sites)

### [timestudy](/projects/timestudy) <span class="small">(Rust)</span>
Frederick W. Taylor was a bastard, but you aren't.

### broken-links <span class="small">(Rust)</span>

Find all broken links from a starting url.\
[repository](https://codeberg.org/kdwarn/broken-links) | [crates.io](https://crates.io/crates/broken-links)

### flashcards <span class="small">(Python)</span>

Create and study flashcards on the command line.\
[repository](https://codeberg.org/kdwarn/flashcards)

### What You've Read <span class="small">(Python)</span>

What You've Read: A bit web bookmarking, a bit reference manager.\
[repository](https://codeberg.org/kdwarn/wyr)

### Don't Forget the Python <span class="small">(Python)</span>

Command-line interface for Remember the Milk.\
[repository](https://codeberg.org/kdwarn/dont-forget-the-python) | [PyPI](https://pypi.org/project/dftp/)

### kdwarn-api <span class="small">(Rust)</span>

We're getting meta. [kdwarn-api](https://codeberg.org/kdwarn/kdwarn-api) is an HTTP REST API for useful and exploratory things. It is both hosted on this website and also enables automated deployment of this website via a webhook and Ansible playbook. It's built with [Dropshot](https://docs.rs/dropshot) and uses Swagger UI for the [docs](https://kdwarn.net/api/docs/). 

### kdwarn.net <span class="small">(Rust)</span>

Meta-er. This site has gone through four iterations so far. Currently, it is a server-side rendered app, using [Leptos](https://leptos.dev). Prior to this, it used [Zola](https://codeberg.org/kdwarn/kdwarn.net/src/tag/zola-0.2), [Dioxus](https://codeberg.org/kdwarn/kdwarn.net/src/tag/dioxus), and [Django](https://codeberg.org/kdwarn/kdwarn.net/src/tag/django). The Ansible configuration to create it is [here](https://codeberg.org/kdwarn/kdwarn.net/src/branch/main/ansible).
