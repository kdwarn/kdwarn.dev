# Timestudy CLI

Timestudy CLI is a tool to track your activities on the command line, heavily inspired by [Timewarrior](https://timewarrior.net/).¹ If you are already a regular user of the command line and want to track your work hours, your personal stuff, or both, timestudy can help you do that. For instance:

[![asciicast](https://asciinema.org/a/596491.svg)](https://asciinema.org/a/596491)

### Installation

For now, "`cargo install ts-cli`". In the future I'll add additional methods.

### Usage

Once installed, "`ts`" is the command to run the program. "`ts help`" will help you learn your way around it. 

Below is what you can do with it so far. The underlined words are the subcommands which can be run, e.g. "`ts status`".

  * show current tracking <u>status</u>
  * <u>start</u> a new activity
  * <u>stop</u> the current activity
  * <u>cancel</u> the current activity
  * <u>continue</u> a previous activity
  * <u>track</u> an activity that already happened
  * <u>modify</u> the start or end time of an activity
  * <u>delete</u> an activity
  * <u>tag</u> activities
  * <u>untag</u> activities
  * show <u>tags</u> used, and their count
    * sorted alphabetically (default)
    * sorted by count
    * unarchived tags (default)
    * archived tags
  * <u>rename-tag</u> (throughout all activities)
  * <u>delete-tag</u> (throughout all activities)
  * <u>archive-tag</u>
  * <u>unarchive-tag</u>
  * <u>describe</u> an activity
  * show a <u>summary</u> of activities
    * all tags
    * specific tags
    * exclude tags
    * within some time period
    * by words within descriptions (exact match - though case-insensitive - for now)
    * with stats
  * <u>generate</u> shell completions

You can learn more about each of these, and see examples, by utilizing the help, e.g. "`ts help start`".

### Development

If you're interested in checking out the code or seeing the plans ahead, visit <https://codeberg.org/kdwarn/ts-cli>.

<hr>

**Endnotes**<br>
<span class="smaller">
¹ If you've used Timewarrior before, using timestudy should feel pretty familiar. So why create it in the first place? Because I like Timewarrior and thought recreating it would be a good way to hone my skills in Rust. I first focused on replicating all of the basic functionality I used, and after that I began to eliminate little papercuts that arose and to polish the experience. As time goes on, I'll likely add additional features I find useful and timestudy will diverge from its Timewarrior roots. I am *not* trying to make a full clone of Timewarrior. I have no plans to implement things like its subcommands "`gaps`", "`join`", "`lengthen`", "`shorten`", or "`split`". I'm looking for a bit more of a simplified tool that I enjoy developing and using. I also want to explore building [a GUI app](https://codeberg.org/kdwarn/ts-gui), and so I first created a [library](https://docs.rs/timestudy/latest/timestudy/) that any application can utilize. (If you would like to develop your own application, you are welcome to do so! The library is licensed under the GPL.) I am not claiming that Timestudy is better or even as good as Timewarrior, but instead that it is an alternative that perhaps some people might want to use.
</span>
