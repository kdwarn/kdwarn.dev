+++
title = "Somewhere amongst the Chaos"
date = "2010-12-21"
+++

Somewhere amongst the chaos
Of
Self-loathing and boredom
Ambition and yearning
Loneliness and hatred
Love and joy
Work and play
Desire and longing
Sleep and routine
Smiles and flirtations
Fear and confusion
Pleasure and pain
Cigarette smoke and drunkenness
Small talk and philosophical conversations
Stifling phone calls and liberating pauses
Muggings and murders
Wars and elections
Epiphanies and plans

Somewhere amongst the chaos
There's order to be found.
