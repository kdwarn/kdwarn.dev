+++
title = "World in Shambles"
date = "2023-10-19"
+++

The world is in shambles
Maybe it's always been this way
But it feels new
It feels like we are unmoored and careening
Toward something we cannot recover from
That all the hope that things would get better
Is fading away

It's always been terrible
For most people
And certainly for most animals

It seemed like there was a chance
That momentum was in the right direction
The direction towards good
Towards liberation, emancipation, freedom
For all things

Was that real?
Or was it just youth-skewed vision?
Was there really a chance?
Has it been lost?
Or is it just middle-aged angst?

A constant rightward lurch for the past century
With some great counterattacks
Against racism, sexism, homophobia
Subjugation of animals
Destruction of the environment

It feels like those of us who escape the worst trauma
Close our eyes
Try to forget
And carry on

Even those of us who fight
Or used to fight

There are beautiful moments, to be sure
But kids get murdered every day
Little precious kids who just want to have fun
And be picked up when they are scared
How do their parents carry on?
I don't think I could
What the fuck is wrong with this world?
What are we doing to each other?

Hatred, authoritarianism, and violent destruction
Appear to be on the rise
Accepted by more and more people
Who should fucking know better
I think we're losing
Both the struggle
And the radical vision of past generations
Teetering towards something awful
Worse than we've seen for some time

There is time for things like this
For reflection
For sadness
For trying to make sense of the senseless
But we must recover
Find our strength
And battle those
Who care only for themselves
For fame and fortune
And nothing for the world.
