+++
title = "Let Me Gather Myself Up"
date = "2014-04-01"
# sometime in that month, not sure of specific date
+++

Let me gather myself up
And head on over to your place
I should be there in thirty minutes or so
Ok, see you soon

Let me gather myself up
And think about what kind of job I can get
What kind of job I want
Something that will pay the bills
But something....
More

Let me gather myself up
And think about what kind of person I want to be
The kind of partner I want to be
What things I want to accomplish
What the obit might say
What kind of meaning there will have been
Let me gather myself up
