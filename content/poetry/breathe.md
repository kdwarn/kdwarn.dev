+++
title = "Breathe"
date = "2011-11-01"
# written sometime this month, not sure of specific date.
+++

Yesterday it smelled like winter and today it feels like it

Winter means the return of the R Street wind
Downshifting a gear or two to push through it
Clothes that don't quite seem warm enough at the start
Sometimes entirely too hot at the end
But at least I have warm clothes to wear
And so I drop a buck or two into the guy's cup

Winter means I've been here for three years now
It shouldn't surprise me but it kinda does
These stairs used to get me so winded
But now I can't believe I was so out of shape
Of course I used to smoke back then
What a difference a few thousand cigarettes make

Winter means that the heat is blasting
Inside the small vestibule before you get into the office
And things are a little quieter than they used to be
Only a little though, and so the headphones stay on
I have yet to master the ability to concentrate
When conversations are going on all around me

Winter means it's getting dark earlier and earlier
And soon it will be dark and I'll be on my way back home
I bike slower now than I used to 
I don't know what my rush was before
Saving some minutes on the commute doesn't matter
If you let yourself breathe in the life all around you

Today it feels like winter and tomorrow it will too.
