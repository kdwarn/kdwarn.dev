+++
title = "The View from My Back Porch"
date = "2010-09-28"
+++

I want to go out
Onto my back porch
And look out across my lawn
To the ferns
And then to the trees
Where birds are chirping
And squirrels are scampering and chattering
Beneath a big blue sky
But I don't have a back porch
Nor a front porch
Just third story windows
Looking out to other
Third story windows
And pavement.
