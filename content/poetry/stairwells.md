+++
title = "Stairwells"
date = "2010-10-06"
+++

There's two elevators
Running up the middle
Of my apartment building
And two sets of stairs,
One on either side

The stairwell closest to me
Goes down to the first floor
While the other goes all the way down
To the basement
Where the laundry room is
With three washers
And three dryers
And no coin machine

When I get around to doing laundry -
When I'm out of clean shirts and underwear -
I take the far set of stairs
To get a little exercise
And not feel guilty
About being lazy and taking the elevator

But sometimes, on the way back up,
I'd lose count of the floors and
Find myself on the 2nd or 4th
Indistinguishable from my 3rd
Except for doormats
In front of the wrong doors
And then the wrong number
Facing the elevator
If I was lost in thought
And made it that far

Until one day I found markings
To the 3rd floor door
That make it easy to recognize:
Half the print of a shoe
Two steps from the landing
Apparently too strong to be washed away
By a simple mopping
And also what appears to be
An old, bloody hand print
In the stucco right next to the door
Which trickled down the wall
When fresh and red and syrupy
Though it's now orange-red and faded

So now those are what I look for
When heading back to my apartment
And every time I see them
It reminds me that this building
Has a past -
One older than my own -
And I wonder about all the things
That have happened here
All the beautiful and terrible things.
