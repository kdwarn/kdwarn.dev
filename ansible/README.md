# Ansible Configuration for kdwarn.net

## Local/development

For local/development, whether the initial setup or the 50th, just run `ansible-playbook playbook.yaml`. There's no need to specify the local inventory as it's in the ansible.cfg file. And the inventory file specifies the user.

Using the domain "kdwarn.localhost" in inventories/local-personal.yaml and the port forwarding to 8042 in the Vagrantfile, the site can be reached locally at http://kdwarn.localhost:8042 (the http is necessary!). This is useful for putting multiple sites on the same vm (mirroring multiple domains on the same VPS).

## Remote/production

For remote/production, the inventory will need to be included: `ansible-playbook playbook.yaml -i inventories/digitalocean.yaml`. For the initial setup, `-e "ansible_user=root"`` will need to be included, as the regular user will not have been created yet. After that, no need to include user, as it's in the inventory file. (This depends on the user's ssh key being added as root when the server is provisioned.)

