---
- name: Include secrets
  include_vars: secrets.yaml

# note that's there's a specific order of database/clone/database stuff here:
# first we create the database/set up db user/privs
# second we get the repo
# third we possibly run migrations contained in the repo
# finally update privs

- name: Create database
  become_user: postgres
  postgresql_db:
    name: "{{ db_name }}"
    state: present
  notify: restart postgres

- name: Create api user for database
  become_user: postgres
  postgresql_user:
    db: "{{ db_name }}"
    name: "{{ db_user }}"
    password: "{{ db_user_pass }}"
  notify: restart postgres

- name: Grant api user CONNECT privileges to db
  become_user: postgres
  postgresql_privs:
    db: "{{ db_name }}"
    role: "{{ db_user }}"
    type: database
    priv: CONNECT
  notify: restart postgres

- name: Grant api user USAGE privileges to db schema
  become_user: postgres
  postgresql_privs:
    db: "{{ db_name }}"
    role: "{{ db_user }}"
    type: schema
    objs: public
    priv: USAGE
  notify: restart postgres

- name: "Clone {{ app_short_name }} repository"
  git:
    repo: "{{ repo }}"
    dest: "{{ app_dir }}"
    version: main
    accept_hostkey: true
  register: repo_status
  notify: "restart {{ app_long_name }} daemon"

- name: Create .env
  copy:
    dest: "{{ app_dir }}/.env"
    content: |
      AUTH="{{ AUTH }}"
      DATABASE_URL="postgres://{{ db_user }}:{{ db_user_pass }}@localhost:5432/{{ db_name }}"
    mode: 0640
  register: config_file
  notify: "restart {{ app_long_name }} daemon"

# Rust/cargo is already installed via the kdwarn.net role
- name: Install sqlx-cli
  become_user: root
  command:
    cmd: cargo install sqlx-cli --no-default-features --features postgres
  environment:
    PATH: /root/.cargo/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
  changed_when: false

- name: Run database migrations
  become_user: root
  command:
    cmd: sqlx migrate run --database-url=postgres://{{ db_user }}:{{ db_user_pass }}@localhost:5432/{{ db_name }}
    chdir: "{{ app_dir }}"
  environment:
    PATH: /root/.cargo/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
  register: migrations
  changed_when: migrations.stdout != ""
  notify: "restart {{ app_long_name }} daemon"

- name: Grant api user SELECT privileges to db schema tables
  become_user: postgres
  postgresql_privs:
    db: "{{ db_name }}"
    role: "{{ db_user }}"
    type: table
    schema: public
    objs: ALL_IN_SCHEMA
    priv: SELECT
  notify: restart postgres

- name: "Compile {{ app_short_name }}"
  command:
    cmd: cargo build --release
    chdir: "{{ app_dir }}"
  environment:
    PATH: /root/.cargo/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
  when: repo_status.changed or config_file.changed or migrations.stdout != ""
  notify: "restart {{ app_long_name }} daemon"

- name: Add Swagger UI static files
  copy:
    src: dist/
    dest: "{{ app_dir }}/dist/"

- name: Add Swagger UI initializer, which contains a variable
  template:
    src: dist/swagger-initializer.js.j2
    dest: "{{ app_dir }}/dist/swagger-initializer.js"
  notify: "restart {{ app_long_name }} daemon"

- name: Upload nginx location configuration 
  template:
    src: "{{ nginx_location_conf_file }}"
    dest: "/etc/nginx/locations/kdwarn/{{ app_short_name }}.conf"
    mode: 0644
  notify: reload nginx

- name: Config the daemon
  template:
    src: "{{ daemon_config_src }}"
    dest: "{{ daemon_config_dest }}"
    mode: 0644
  notify: "restart {{ app_long_name }} daemon"

- name: "Start the daemon (first time)"
  systemd:
    name: "{{ app_short_name }}.service"
    state: started
    enabled: yes
    daemon_reload: yes

